<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Models\Meta;

class Helper {

    public static function api_output($msg,$data) {
        $output = [
            'msg' => $msg,
            'data' => $data
        ];
        return $output;
    }

    public static function setting_website()
    {
        return Meta::where('purpose','website_setting')->get(['purpose','key','value']);
    }

    public static function image_upload($file,$file_path='',$name='')
    {
        $file_original_name = $name . "." . $file->getClientOriginalExtension();
        $imgData = Image::make($file);
        $file_name = $file_path . '/' . $file_original_name;
        $theImage = $imgData->stream()->__toString();
        Storage::disk('public')->put($file_name, $theImage, 'public');
        return $file_original_name;
    }
}

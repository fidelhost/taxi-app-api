<?php

namespace App\Helpers;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Project\App\Models\Meta;
use \Firebase\JWT\JWT;
/**
 * Api output with message and data.
 * @since 1.0.0
 * @author kamal
 */
if (!function_exists('api_output')) {
    function api_output($msg,$data) {
        $output = [
            'msg' => $msg,
            'data' => $data
        ];
        return $output;
    }
}


/**
 * Api output with message and data.
 * @since 1.0.0
 * @author kamal
 */
if (!function_exists('the_auth_id')) {
    function the_auth_id() {
        $token = request()->header('Jwt-Token');
        $key = "jwt_key";
        try {
            $jwt = JWT::decode($token, $key, array('HS256'));
            $payload =  (array) $jwt;
            return  [
                    'success' => true,
                    'id' => $payload['tid'],
                    'type' => $payload['ut']
                ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }
    }
}

if (!function_exists('setting_website')) {
    function setting_website()
    {
        return Meta::where('purpose','website_setting')->get(['purpose','key','value']);
    }
}


if (!function_exists('image_upload')) {
    function image_upload($file,$file_path='',$name='')
    {
        $file_original_name = $name . "." . $file->getClientOriginalExtension();
        $imgData = Image::make($file);
        $file_name = $file_path . '/' . $file_original_name;
        $theImage = $imgData->stream()->__toString();
        Storage::disk('public')->put($file_name, $theImage, 'public');
        return $file_original_name;
    }
}

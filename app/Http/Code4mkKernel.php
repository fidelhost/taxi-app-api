<?php

namespace App\Http;

class Code4mkKernel
{
  function middleware(){
    $middleware = [
      //\App\Http\Middleware\Me::class,

    ];
    return $middleware;
  }

  function routeMiddleware(){
    $routerMiddlware = [
      'is_auth' => \Project\App\Middleware\Uauth::class,
      'is_authorized' => \Project\App\Middleware\Credential::class,
    ];
    return $routerMiddlware;
  }

  function prependMiddlewareGroup(){
    $middlewareGroup = [
      //'web' => \Parrot\Parrot\Middleware\Name::class,
    ];
    return $middlewareGroup;
  }

  function MiddlewareGroup(){
    $middlewareGroup = [
      'the_api' => [
        \Project\App\Middleware\Credential::class,
        'throttle:60,1',
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
      ]
    ];
    return $middlewareGroup;
  }

}

?>

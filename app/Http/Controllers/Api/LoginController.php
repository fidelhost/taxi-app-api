<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use \Firebase\JWT\JWT;
use DateInterval;
use Validator;
use DateTime;
use Hash;

class LoginController extends Controller
{

    public function register(Request $request)
    {
        $validate = [];
        if($request->email){
            // $validateData['email'] = ['required'];
            $usernameData = [
                'email' => 'required|unique:users',
                'phone' => 'unique:users'
            ];
        }else{
            $usernameData = [
                'phone' => 'required|unique:users',
                'email' => 'unique:users',
            ];
        }
        $validate = array_merge($validate,$usernameData);
        $validateData = [
            'password' => 'required|min:8'
        ];
        $validateData = array_merge($validate,$validateData);
        $validator = Validator::make($request->all(),$validateData);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        // User::forceCreate([
        //     'email' => $request->email,
        //     'phone' => $request->phone,
        //     'password' => bcrypt($request->password),
        //     'type' => 'customer'
        // ]);
    }
    public function login(Request $request)
    {
        $user = User::where('email',$request->username)
                     ->orWhere('phone',$request->username)
                     ->first();
        if ($user) {
            if (Hash::check($request->password,$user->password)) {
                $token = $this->token();
                $msg = ['status' => 'success', 'message' => __('Login successfully'), 'success' => true];
                $data = [
                    'jwt_token'=> $token,
                    'user' => $user
                 ];
                return response()->json(Helper::api_output($msg,$data),200);
            }else{
                $token = $this->token();
                $msg = ['status' => 'error', 'message' => __('Your username or password is wrong'), 'success' => false];
                return response()->json(Helper::api_output($msg,[]),200);
            }
        }else{
            $token = $this->token();
            $msg = ['status' => 'error', 'message' => __('Your username or password is wrong'), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

    }

    public function authID(Request $request)
    {
        $token = $request->header('Jwt-Token');
        $key = "jwt_key";
        try {
            $jwt = JWT::decode($token, $key, array('HS256'));
            $payload =  (array) $jwt;
            return  [
                    'success' => true,
                    'id' => $payload['tid'],
                    'type' => $payload['ut']
                ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }

    }

    public function authUser(Request $request)
    {
        $authID =  $this->authID($request);
        if($authID['success']){
            $u = User::where('id',$authID['id'])->first();
            return $u;
        }
    }

    private function token()
    {
        $issueDate = new DateTime();
        $expiredDate = new DateTime();
        $tokenDuration = 'P32D';
        $jwtIss = 'taxiapp';
        $jwtAud = 'taxiapp';
        $expiredDate->add(new DateInterval($tokenDuration));
        $key = "jwt_key";
        $token = array(
            "iss" => $jwtIss,
            "ut" => 'mk',
            "iat" => $issueDate->getTimestamp(),
            "expM" => $expiredDate->getTimestamp(),
            "tid" => '1',
        );
        $jwt = JWT::encode($token,$key);

        return $jwt;
    }
}

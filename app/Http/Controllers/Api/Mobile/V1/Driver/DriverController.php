<?php

namespace App\Http\Controllers\Api\Mobile\V1\Driver;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use \Firebase\JWT\JWT;
use DateInterval;
use DateTime;
use Hash;

class DriverController extends Controller
{
    public function register(Request $request)
    {
        $validate = [];
        if($request->email){
            // $validateData['email'] = ['required'];
            $usernameData = [
                'email' => 'required|unique:users',
                'phone' => 'unique:users'
            ];
        }else{
            $usernameData = [
                'phone' => 'required|unique:users',
                'email' => 'unique:users',
            ];
        }
        $validate = array_merge($validate,$usernameData);
        $validateData = [
            'password' => 'required|min:8'
        ];
        $validateData = array_merge($validate,$validateData);
        $validator = Validator::make($request->all(),$validateData);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $user = User::forceCreate([
            'name' => $request->first_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'is_approve' => false,
            'type' => 'driver'
        ])->id;

        if($user){
            $did = Driver::forceCreate([
                'user_id' => $user,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'gender' => $request->gender,
                'nid_no' => $request->nid,
                'referral_code' => $request->referral_code
            ])->id;
             User::where('id',$user)->update([
                'sid' => $did
            ]);
        }


        $msg = ['status' => 'success', 'message' => __('Registration  complete'), 'success' => true];
        return response()->json(Helper::api_output($msg,$user),200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'username' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $user = User::with(['driver'])->where('email',$request->username)
                     ->orWhere('phone',$request->username)
                     ->first();
        if ($user) {
            if (Hash::check($request->password,$user->password)) {
                $token = $this->token($user->id);
                $msg = ['status' => 'success', 'message' => __('Login successfully'), 'success' => true];
                $data = [
                    'jwt_token'=> $token,
                    'user' => $user->makeHidden('password')
                 ];
                return response()->json(Helper::api_output($msg,$data),200);
            }else{
                $msg = ['status' => 'error', 'message' => __('Your username or password is wrong'), 'success' => false];
                return response()->json(Helper::api_output($msg,[]),200);
            }
        }else{
            $msg = ['status' => 'error', 'message' => __('Your username or password is wrong'), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
    }

    public function authID(Request $request)
    {
        $token = $request->header('Jwt-Token');
        $key = "jwt_key";
        try {
            $jwt = JWT::decode($token, $key, array('HS256'));
            $payload =  (array) $jwt;
            return  [
                    'success' => true,
                    'id' => $payload['tid'],
                    'type' => $payload['ut']
                ];
        } catch (\Exception $e) {
            return [
                'success' => false,
                'msg' => $e->getMessage()
            ];
        }

    }

    public function authUser(Request $request)
    {
        $authID =  $this->authID($request);
        if($authID['success']){
            $u = User::with(['driver'])->where('id',$authID['id'])->first();
            return $u;
        }
    }

    private function token($id)
    {
        $issueDate = new DateTime();
        $expiredDate = new DateTime();
        $tokenDuration = 'P32D';
        $jwtIss = 'taxiapp';
        $jwtAud = 'taxiapp';
        $expiredDate->add(new DateInterval($tokenDuration));
        $key = "jwt_key";
        $token = array(
            "iss" => $jwtIss,
            "ut" => 'mk',
            "iat" => $issueDate->getTimestamp(),
            "expM" => $expiredDate->getTimestamp(),
            "tid" => $id,
        );
        $jwt = JWT::encode($token,$key);

        return $jwt;
    }

}

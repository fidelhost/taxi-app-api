<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Country;
use App\Models\ServiceArea;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class AreaController extends Controller
{
    public function show($uuid){
        $area = ServiceArea::where('id',$uuid)->first();
        if($area){
            $area->normal_vehicle_type = json_decode($area->normal_vehicle_type);
            $area->rental_vehicle_type = json_decode($area->rental_vehicle_type);
            $area->outstation_vehicle_type = json_decode($area->outstation_vehicle_type);
            $area->vehicle_document_id = json_decode($area->vehicle_document_id );
            $area->driver_document_id = json_decode($area->driver_document_id);
            $area->rental_package = json_decode($area->rental_package);
        }
        $msg = ['status' => 'success', 'message' => __($area->name . '  retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$area),200);
    }

    public function get(Request $request)
    {

        if($request->id){
            $area = ServiceArea::where('id',$request->id)->first();
            if($area){
                $area->normal_vehicle_type = json_decode($area->normal_vehicle_type);
                $area->rental_vehicle_type = json_decode($area->rental_vehicle_type);
                $area->outstation_vehicle_type = json_decode($area->outstation_vehicle_type);
                $area->vehicle_document_id = json_decode($area->vehicle_document_id );
                $area->driver_document_id = json_decode($area->driver_document_id);
                $area->rental_package = json_decode($area->rental_package);
            }
            $msg = ['status' => 'success', 'message' => __($area->name . '  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$area),200);
        }
        if($request->show_all == 'all'){
            $area = ServiceArea::all();
            $msg = ['status' => 'success', 'message' => __('area retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$area),200);
        }
        $per_page = $data->per_page ?? 50;
        $areas = ServiceArea::with(['country'])->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('area retrive successfully'), 'success' => true, 'data' => $request->all()];
        return response()->json(Helper::api_output($msg,$areas),200);

    }

    public function createArea(Request $request)
    {

        //print_r($request); exit;

        //$data = $request->all();
        //return "Hello";
        // return response()->json($request->area);
        $area = new ServiceArea();
        $area->name = $request->name;
        $area->country_id = $request->country_id;
        $area->is_airport = $request->is_airport == true ? true : false;
        $area->normal_vehicle_type = json_encode($request->normal_vehicle_type);
        $area->rental_package = json_encode($request->rental_package);
        $area->rental_vehicle_type =json_encode($request->rental_vehicle_type) ;
        $area->outstation_vehicle_type = json_encode($request->outstation_vehicle_type);
        $area->is_pool = $request->is_pool;
        $area->pool_position = $request->pool_position;
        $area->vehicle_document_id = json_encode($request->vehicle_document_id);
        $area->driver_document_id = json_encode($request->driver_document_id);
        $area->driver_wallet_min_balance = $request->driver_wallet_min_balance;
        // $area->is_auto_upgrate = $request->is_auto_upgrate;
        $area->driver_bill_period = $request->driver_bill_period;
        $area->driver_bill_start_time = $request->driver_bill_start_time;
        $area->driver_bill_start_day = $request->driver_bill_start_day;
        $area->driver_bill_start_date = $request->driver_bill_start_date;
        // $area->polygon = json_encode($request->polygon);
        $area->save();
        $msg = ['status' => 'success', 'message' => __('Area create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$area),200);
    }

    public function updateArea(Request $request)
    {

        $data = (object) $request->area;
        // return response()->json($request->area);
        $area = ServiceArea::where('id',$request->id)->first();
        $area->name = $data->name;
        $area->country_id = $data->country_id;
        $area->is_airport = $data->is_airport;
        $area->normal_vehicle_type = json_encode($data->normal_vehicle_type);
        $area->rental_package = json_encode($data->rental_package);
        $area->rental_vehicle_type =json_encode($data->rental_vehicle_type) ;
        $area->outstation_vehicle_type = json_encode($data->outstation_vehicle_type);
        $area->is_pool = $data->is_pool;
        $area->pool_position = $data->pool_position;
        $area->vehicle_document_id = json_encode($data->vehicle_document_id);
        $area->driver_document_id = json_encode($data->driver_document_id);
        $area->driver_wallet_min_balance = $data->driver_wallet_min_balance;
        $area->is_auto_upgrate = $data->is_auto_upgrate;
        $area->driver_bill_period = $data->driver_bill_period;
        $area->driver_bill_start_time = $data->driver_bill_start_time;
        $area->driver_bill_start_day = $data->driver_bill_start_day;
        $area->driver_bill_start_date = $data->driver_bill_start_date;
        $area->save();
        $msg = ['status' => 'success', 'message' => __('Area update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$area),200);
    }

    public function action(Request $request)
    {
        if($request->action == 'status'){
            $country = ServiceArea::where('id',$request->id)->first();
            $country->is_active = $request->status == 1 ? false : true;
            $country->save();
            $msg = ['status' => 'success', 'message' => __("Area status change successfully"), 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }elseif ($request->action === 'delete') {
            $country = ServiceArea::where('id',$request->id)->delete();
            $msg = ['status' => 'success', 'message' => __("Area delete successfully"), 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }

    }
}

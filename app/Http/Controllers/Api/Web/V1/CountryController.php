<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class CountryController extends Controller
{

    public function get(Request $request)
    {
        if($request->id){
            $country = Country::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __($country->name . '  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$country),200);
        }
        if($request->show_all == 'all'){
            $country = Country::all();
            $msg = ['status' => 'success', 'message' => __('country retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$country),200);
        }
        $per_page = $request->per_page ?? 50;
        $countrys = Country::orderBy('id','asc')->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Countries retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$countrys),200);

    }

    public function createCountry(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:countries',
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        $country = new Country();
        $country->name = $request->name;
        $country->code = $request->code;
        $country->currency = $request->currency;
        $country->currency_code = $request->currency_code;
        $country->default_lang = $request->lang;
        $country->distance_unit = $request->distance_unit;
        $country->min_phone_digit = $request->min_phone_digit;
        $country->max_phone_digit = $request->max_phone_digit;
        $country->sequence = $request->sequence;
        $country->save();
        $msg = ['status' => 'success', 'message' => __('Country create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$country),200);
    }

    public function updateCountry(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:countries,name,'.$request->name.',name',
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        $country = Country::where('id',$request->id)->first();
        $country->name = $request->name;
        $country->code = $request->code;
        $country->currency = $request->currency;
        $country->currency_code = $request->currency_code;
        $country->default_lang = $request->lang;
        $country->distance_unit = $request->distance_unit;
        $country->min_phone_digit = $request->min_phone_digit;
        $country->max_phone_digit = $request->max_phone_digit;
        $country->sequence = $request->sequence;
        $country->save();
        $msg = ['status' => 'success', 'message' => __('Country update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$country),200);
    }

    public function action(Request $request)
    {
        if($request->action == 'status'){
            $country = Country::where('id',$request->id)->first();
            $country->is_active = $request->status == 1 ? false : true;
            $country->save();
            $msg = ['status' => 'success', 'message' => __("Country status change successfully"), 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }elseif ($request->action === 'delete') {
            $country = Country::where('id',$request->id)->delete();
            $msg = ['status' => 'success', 'message' => __("Country delete successfully"), 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }

    }

}

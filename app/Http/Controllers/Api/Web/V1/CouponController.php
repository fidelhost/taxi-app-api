<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Country;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class CouponController extends Controller
{

    public function get(Request $request)
    {
        if($request->name){
            $coupon = Page::where('name',$request->name)->first();
            $msg = ['status' => 'success', 'message' => __($coupon->name . '  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$coupon),200);
        }
        $per_page = $request->per_page ?? 50;
        $coupons = Coupon::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Countries retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$coupons),200);

    }

    public function createCoupon(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'code' => 'required|unique:coupons',
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        $coupon = new Coupon();
        $coupon->code = $request->code;
        $coupon->amount = $request->amount;
        $coupon->type = 'per';
        $coupon->max_amount = $request->max_amount;
        $coupon->quantity = $request->quantity;
        $coupon->per_user_quantity = $request->per_user_quantity;
        $coupon->active_on = $request->active_on;
        $coupon->save();
        $msg = ['status' => 'success', 'message' => __('Coupon create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$coupon),200);
    }

    public function updatePage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:pages,slug,'.$request->slug.',slug',
            'title' => 'required',
            'details' => 'required'
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        Page::where('slug','=','about')->update(['name'=>$request->name,'slug'=>Str::slug($request->slug),'title'=>$request->title,'text'=>$request->details]);
        $msg = ['status' => 'success', 'message' => __('Page update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$request->slug),200);
    }

    public function getRoles(Request $request)
    {

        if ($request->role) {
            $role = Role::with(['users','permissions'])->where('slug',$request->role)->first();
            $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$role),200);
        }

        $roles = Role::paginate(200);
        $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$roles),200);
    }

    public function assignRole(Request $request)
    {
        RoleUser::forceCreate([
            'user_id' => $request->user_id,
            'role_id' => $request->role_id
        ]);
        $msg = ['status' => 'success', 'message' => __('Role assign successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }
}

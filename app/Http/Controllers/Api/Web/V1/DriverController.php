<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class DriverController extends Controller
{
    public function getDrivers(Request $request)
    {
        $where = [];
        if($request->get_type == 'approval') {
            $where['is_approve'] = false;
        }elseif ($request->get_type == 'rejected') {
            $where['is_reject'] = true;
        }
        $per_page = $request->per_page ?? 10;
        $drivers = User::with(['driver'])->where('type','driver')->where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Driver retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$drivers),200);
    }

    public function action(Request $request)
    {
        $data = [];
        $msg = ['status' => 'success', 'message' => __('nothing'), 'success' => true];

        if($request->action_type == 'approve'){
            $data['is_approve'] = true;
            $data['is_reject'] = false;
            $msg = ['status' => 'success', 'message' => __('Driver approved  successfully'), 'success' => true];
        }else if($request->action_type == 'reject'){
            $data['is_reject'] = true;
            $data['is_approve'] = false;
            $msg = ['status' => 'success', 'message' => __('Driver rejected  successfully'), 'success' => true];
        }else if($request->action_type == 'ban'){
            $data['is_ban'] = true;
            $msg = ['status' => 'success', 'message' => __('Driver ban  successfully'), 'success' => true];
        }else if($request->action_type == 'unban'){
            $data['is_ban'] = false;
            $msg = ['status' => 'success', 'message' => __('Driver unban  successfully'), 'success' => true];
        }
        $diver = User::where('id',$request->id)->update($data);

        return response()->json(Helper::api_output($msg,[]),200);
    }
}

<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Trans;
use App\Models\Lang;
use Illuminate\Http\Request;
use Validator;
use File;

class I18nController extends Controller
{
    public function createLang(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'short' => 'required|unique:langs,short,'.$request->short.',short',
            'code' => 'required|unique:langs,code,'.$request->code.',code',
        ]);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        if(Lang::where('code',$request->code)->first()){
            $lang = Lang::where('code',$request->code)->first();
        }else{
            $lang = new Lang();
        }

        $lang->name = $request->name;
        $lang->short = $request->short;
        $lang->code = $request->code;
        $lang->save();

        $this->prepareStoreNow('core');
        $this->prepareStoreNow($lang->code);
        $this->generateTrans($lang->code);

        $msg = ['status' => 'success', 'message' => __('Language create/update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$lang),200);
    }

    public function get(Request $request)
    {
        if ($request->code) {
            $lang = Lang::where('code',$request->code)->first();
            $msg = ['status' => 'success', 'message' => __($lang->name . ' Language retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$lang),200);
        }
        $per_page = $request->per_page ?? 50;
        $langs = Lang::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Language retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$langs),200);
    }

    public function activity(Request $request){
        $theLang = Lang::where('code',$request->code)->first();
        if ($request->process == 'active') {
            $lang = Lang::where('code',$request->code)->update([
                'is_active' => true
            ]);
            $msg = ['status' => 'success', 'message' => __(':name Language active successfully',['name' => $theLang->name]), 'success' => true];
        }elseif ($request->process == 'deactive') {
            $lang = Lang::where('code',$request->code)->update(['is_active' => false]);
            $msg = ['status' => 'success', 'message' => __(':name Language deactive successfully',['name' => $theLang->name]), 'success' => true];
        }elseif ($request->process == 'delete') {
            $lang = Lang::where('code',$request->code)->delete();
            $msg = ['status' => 'success', 'message' => __(':name Language delete successfully',['name' => $theLang->name]), 'success' => true];
        }
        return response()->json(Helper::api_output($msg,[]),200);
    }

    // translate

    public function getTheTranslations(Request $request)
    {
        $this->prepareStoreNow('core');
        $this->prepareStoreNow($request->code);

        $core = Trans::where('lang_code','core')->get();
        $desired = Trans::where('lang_code',$request->code)->get();
        if (true) {
            $data = ['core' => $core,'desired' => $desired];
            $msg = ['status' => 'success', 'message' => __('Translate retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$data),200);
        }
    }

    public function updateTheTranslations(Request $request)
    {
        foreach ($request->desired as $key => $lang) {
            $theLang = json_decode($lang);
            Trans::where('key',$theLang->key)
                            ->where('lang_code',$request->lang)
                            ->update([
                                "key"       => $theLang->key,
                                "text"      => $theLang->text,
                                "lang_code" => $request->lang,
                                "group"     => $theLang->group,
                                "sub_group" => $theLang->sub_group,
                                "device"    => $theLang->device
                            ]);
        }
        $msg = ['status' => 'success', 'message' => __('Translate upldate successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function generateJson(Request $request)
    {
        $theLang = Lang::where('code',$request->code)->first();
        $this->prepareStoreNow('core');
        $this->prepareStoreNow($request->code);
        $this->generateTrans($request->code);
        $msg = ['status' => 'success', 'message' => __(':name translate generate  successfully',['name' => $theLang->name]), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    private function generateTrans($lang)
    {
        $checkLang = Trans::where('lang_code',$lang)->first();
        if ($checkLang) {
            $insertPath = resource_path('lang/'.$lang.'.json');
           if(File::exists($insertPath)){
               File::delete($insertPath);
           }
           // generate key => value from db
           $generatedData = [];
           $i18n = Trans::where('lang_code',$lang)->get();
           foreach ($i18n as $key => $value) {
               $generatedData[$value->key] = $value->text;
           }
           // generate and save the resource folder
           File::put($insertPath,json_encode($generatedData,JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
        }
    }

    private function  prepareStoreNow($lang)
    {
        $content = $this->contents();
        foreach ($content as $key => $value) {
            if (!empty($key)) {
                $langData = $this->getTranslateData($key,$value,$lang);
                $this->storeTranslateData($langData,$lang);
            }
        }
    }

    private function storeTranslateData($data,$lang="core")
    {
        if ($lang === 'core') {
            Trans::updateOrCreate(['key'=>$data['key'],'lang_code'=>$data['lang_code']],$data);
        } else {
            $lang2 = Trans::where('key',$data['key'])
                            ->where('lang_code',$data['lang_code'])
                            ->first();
            if ($lang2 === null) {
                Trans::forceCreate($data);
            }
        }
    }

    private function getTranslateData($key,$value,$lang)
    {
        $data = [
            "key"       => $key,
            "text"      => $value["text"],
            "lang_code"      => $lang,
            "group"     => $value["group"],
            "sub_group" => $value["sub_group"],
            "device"    => $value["device"]
        ];
        return $data;
    }

    private function contents()
    {
        $content = config('project-translate');
        return $content;
    }
}

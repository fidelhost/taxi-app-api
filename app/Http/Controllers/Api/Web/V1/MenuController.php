<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\SiteMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class MenuController extends Controller
{

    public function get(Request $request)
    {
        if($request->slug){
            $menu = SiteMenu::where('slug',$request->slug)->first();
            $msg = ['status' => 'success', 'message' => __($menu->name . ' page retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$menu),200);
        }

        if($request->type == 'all'){
            $menu = SiteMenu::with(['parent','child'])->get();
            $msg = ['status' => 'success', 'message' => __( ' Menus retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$menu),200);
        }

        if($request->type == 'all_only'){
            $menu = SiteMenu::all();
            $msg = ['status' => 'success', 'message' => __( ' Menus retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$menu),200);
        }

        $per_page = $request->per_page ?? 50;
        $menus = SiteMenu::with(['parent','child'])->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Pages retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$menus),200);

    }

    public function createMenu(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'slug' => 'required|unique:pages',
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        $menu = new SiteMenu();
        $menu->name = $request->name;
        $menu->slug = Str::slug($request->slug);
        $menu->parent_id = $request->parent_id;
        $menu->active = $request->active ? 1 : 0;
        $menu->save();
        $msg = ['status' => 'success', 'message' => __('Menu create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$menu),200);
    }

    public function updateMenu(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:pages,slug,'.$request->slug.',slug',
            'title' => 'required',
            'details' => 'required'
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        Page::where('slug',$request->slug)->update([
            'name'=>$request->name,
            'slug'=>Str::slug($request->slug),
            'parent_id'=>$request->parent_id,
            'active'=>$request->active
        ]);

        $msg = ['status' => 'success', 'message' => __('Menu update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$request->slug),200);
    }
}

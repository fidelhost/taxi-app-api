<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MetaController extends Controller
{
    public function create(Request $request)
    {
       $data = $request->except(['_token']);

       foreach ($data['the_data'] as $key => $value) {
           Meta::updateOrCreate(
                ['purpose'=>$data['purpose'],'key'=>$key],
                [
                   'purpose' => $data['purpose'],
                   'key' => $key,
                   'value' => $value
                ]
            );
       }
       $msg = ['status' => 'success', 'message' => __( 'Save successfully'), 'success' => true];
       return response()->json(Helper::api_output($msg,[]),200);
    }

    public function get(Request $request)
    {
        $meta = Meta::where('purpose',$request->purpose)->get();

        $msg = ['status' => 'success', 'message' => __( 'Meta retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$meta),200);
    }

}

<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\NavigationDrawer;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class NavigationController extends Controller
{

    public function get(Request $request)
    {
        if($request->id){
            $navigation = NavigationDrawer::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __($navigation->name . ' page retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$navigation),200);
        }
        $per_page = $request->per_page ?? 50;
        $navigations = NavigationDrawer::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Navigation retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$navigations),200);

    }

    public function create(Request $request)
    {
        // $validator = Validator::make($request->all(),[
        //     'name' => 'required',
        //     'description' => 'required|unique:pages',
        //     'title' => 'required',
        //     'details' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
        //     $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
        //     return response()->json(Helper::api_output($msg,[]),200);
        // }
        $navigation = new NavigationDrawer();
        $navigation->name = $request->name;
        $navigation->description = $request->description;
        $navigation->sequence = $request->sequence;
        $navigation->is_active = $request->is_active ?? false;
        $navigation->save();
        $msg = ['status' => 'success', 'message' => __('Navigation create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$navigation),200);
    }

    public function update(Request $request)
    {
        $navigation = NavigationDrawer::where('id',$request->id)->first();
        $navigation->name = $request->name;
        $navigation->description = $request->description;
        $navigation->sequence = $request->sequence;
        $navigation->is_active = $request->is_active;
        $navigation->save();
        $msg = ['status' => 'success', 'message' => __('Navigation update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

}

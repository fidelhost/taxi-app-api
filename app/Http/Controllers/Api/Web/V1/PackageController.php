<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Country;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class PackageController extends Controller
{

    public function get(Request $request)
    {
        if($request->id){
            $package = Package::where('id',$request->id)->first();

            $msg = ['status' => 'success', 'message' => __($package->name . '  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$package),200);
        }
        if($request->show_all == 'all'){
            $package = Package::where('type',$request->type)->orderBy('id','asc')->get();
            $msg = ['status' => 'success', 'message' => __('package retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$package),200);
        }
        $per_page = $data->per_page ?? 50;
        $packages = Package::where('type',$request->type)->orderBy('id','asc')->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('package retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$packages),200);

    }

    public function createPackage(Request $request)
    {
        $package = new Package();
        $package->type = $request->type;
        $package->name = $request->name;
        $package->description = $request->description;
        $package->term_and_condition = $request->term_and_condition;
        $package->save();
        $msg = ['status' => 'success', 'message' => __('package create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$package),200);
    }

    public function updatePackage(Request $request)
    {
        $package = Package::where('id',$request->id)->first();
        $package->name = $request->name;
        $package->description = $request->description;
        $package->term_and_condition = $request->term_and_condition;
        $package->save();
        $msg = ['status' => 'success', 'message' => __('package update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$package),200);
    }

    public function action(Request $request)
    {
        if($request->action == 'status'){
            $country = Package::where('id',$request->id)->first();
            $country->is_active = $request->status == 1 ? false : true;
            $country->save();
            $msg = ['status' => 'success', 'message' => __("Area status change successfully"), 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }elseif ($request->action === 'delete') {
            $country = Package::where('id',$request->id)->delete();
            $msg = ['status' => 'success', 'message' => __("Area delete successfully"), 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }

    }
}

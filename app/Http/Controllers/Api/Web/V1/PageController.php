<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class PageController extends Controller
{

    public function get(Request $request)
    {
        if($request->slug){
            $page = Page::where('slug',$request->slug)->first();
            $msg = ['status' => 'success', 'message' => __($page->name . ' page retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$page),200);
        }
        $per_page = $request->per_page ?? 50;
        $pages = Page::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Pages retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$pages),200);

    }

    public function createPage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:pages',
            'title' => 'required',
            'details' => 'required'
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        $page = new Page();
        $page->name = $request->name;
        $page->slug = Str::slug($request->slug);
        $page->title = $request->title;
        $page->text = $request->details;
        $page->save();
        $msg = ['status' => 'success', 'message' => __('Page create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$page),200);
    }

    public function updatePage(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:pages,slug,'.$request->slug.',slug',
            'title' => 'required',
            'details' => 'required'
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        Page::where('slug','=','about')->update(['name'=>$request->name,'slug'=>Str::slug($request->slug),'title'=>$request->title,'text'=>$request->details]);
        $msg = ['status' => 'success', 'message' => __('Page update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$request->slug),200);
    }

    public function getRoles(Request $request)
    {

        if ($request->role) {
            $role = Role::with(['users','permissions'])->where('slug',$request->role)->first();
            $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$role),200);
        }

        $roles = Role::paginate(200);
        $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$roles),200);
    }

    public function assignRole(Request $request)
    {
        RoleUser::forceCreate([
            'user_id' => $request->user_id,
            'role_id' => $request->role_id
        ]);
        $msg = ['status' => 'success', 'message' => __('Role assign successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }
}

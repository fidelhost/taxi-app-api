<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\RolePermission;
use App\Models\Permission;
use App\Models\RoleUser;
use App\Models\Role;
use Illuminate\Http\Request;
use Validator;

class PermissionController extends Controller
{

    public function get(Request $request)
    {
        $perms = config('project-permissions');
        foreach ($perms as $key => $perm) {
            Permission::updateOrCreate(
                ['slug' => $key],
                ['name' => $perm]
            );
        }
        if($request->show == 'all'){
            $permissions = Permission::all();

            $msg = ['status' => 'success', 'message' => __('Permissions retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$permissions),200);
        }

        $per_page = $request->per_page ?? 50;
        $permissions = Permission::paginate($per_page);

        $msg = ['status' => 'success', 'message' => __('Permissions retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$permissions),200);

    }

    public function createRole(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:roles'
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $role = new Role();
        $role->name = $request->name;
        $role->slug = $request->slug;
        $role->save();

        $msg = ['status' => 'success', 'message' => __('Role create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$role),200);
    }

    public function getRoles(Request $request)
    {

        if ($request->role) {
            $role = Role::with(['users','permissions'])->where('slug',$request->role)->first();
            $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$role),200);
        }
        if($request->show == 'all') {
            $roles = Role::all();
            $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$roles),200);
        }

        $per_page = $request->per_page ?? 50;
        $roles = Role::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$roles),200);
    }

    public function assignRole(Request $request)
    {
        if(RoleUser::where('user_id',$request->user_id)->where('role_id',$request->role_id)->exists()){
            $msg = ['status' => 'success', 'message' => __('This User  already have this role'), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        RoleUser::where('user_id',$request->user_id)->delete();
        RoleUser::forceCreate([
            'user_id' => $request->user_id,
            'role_id' => $request->role_id
        ]);
        $msg = ['status' => 'success', 'message' => __('Role assign successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function assignPermission(Request $request)
    {
        $permissions = $request->permissions;
        RolePermission::where('role_id',$request->role_id)->delete();
        for ($i=0; $i < count($permissions) ; $i++) {
            RolePermission::forceCreate([
                'role_id' => $request->role_id,
                'permission_id' => $permissions[$i]
            ]);
        }

        $msg = ['status' => 'success', 'message' => __('Permission assign successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }
}

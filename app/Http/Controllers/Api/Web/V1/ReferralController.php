<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\ReferralSystem;
use Illuminate\Validation\Rule;
use App\Models\Country;
use App\Models\ServiceArea;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;

class ReferralController extends Controller
{

    public function get(Request $request)
    {
        if($request->id){
            $referral = ReferralSystem::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __($referral->name . '  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$referral),200);
        }
        if($request->show_all == 'all'){
            $referral = ReferralSystem::all();
            $msg = ['status' => 'success', 'message' => __('country retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$referral),200);
        }
        $per_page = $request->per_page ?? 50;
        $referrals = ReferralSystem::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Referral retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$referrals),200);

    }

    public function createRferral(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'country_id' => 'required|unique:referral_systems',
        ]);

        if ($validator->fails()) {
            // $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false]);
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }
        $referral = new ReferralSystem();
        $referral->country_id = $request->country_id;
        $referral->app = $request->app;
        $referral->limit_type = $request->limit_type;
        $referral->number_limit = $request->number_limit;
        $referral->num_days = $request->num_days;
        $referral->day_count = $request->day_count;
        $referral->applicible = $request->applicible;
        // $referral->start_date =
        $referral->offer_type = $request->offer_type;
        $referral->offer_value_type = $request->offer_value_type;
        $referral->offer_value = $request->offer_value;
        $referral->is_active = $request->is_active ? true : false;
        $referral->save();
        $msg = ['status' => 'success', 'message' => __('Referral create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$referral),200);
    }

    public function updateRferral(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'country_id' => 'required|unique:referral_systems,country_id,'.$request->country_id.',country_id',
        ]);

        $referral = ReferralSystem::where('id',$request->id)->first();
        $referral->country_id = $request->country_id;
        $referral->app = $request->app;
        $referral->limit_type = $request->limit_type;
        $referral->number_limit = $request->number_limit;
        $referral->num_days = $request->num_days;
        $referral->day_count = $request->day_count;
        $referral->applicible = $request->applicible;
        // $referral->start_date =
        $referral->offer_type = $request->offer_type;
        $referral->offer_value_type = $request->offer_value_type;
        $referral->offer_value = $request->offer_value;
        $referral->is_active = $request->is_active ? true : false;
        $referral->save();
        $msg = ['status' => 'success', 'message' => __('Referral create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$referral),200);
    }

    public function getRoles(Request $request)
    {

        if ($request->role) {
            $role = Role::with(['users','permissions'])->where('slug',$request->role)->first();
            $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$role),200);
        }

        $roles = Role::paginate(200);
        $msg = ['status' => 'success', 'message' => __('Role retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$roles),200);
    }

    public function assignRole(Request $request)
    {
        RoleUser::forceCreate([
            'user_id' => $request->user_id,
            'role_id' => $request->role_id
        ]);
        $msg = ['status' => 'success', 'message' => __('Role assign successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }
}

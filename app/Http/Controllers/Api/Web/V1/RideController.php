<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Ride;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class RideController extends Controller
{

    public function get(Request $request)
    {
        if($request->name){
            $ride = ReferralSystem::where('name',$request->name)->first();
            $msg = ['status' => 'success', 'message' => __($ride->name . '  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$ride),200);
        }
        if($request->show_all == 'all'){
            $ride = Ride::all();
            $msg = ['status' => 'success', 'message' => __('country retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$ride),200);
        }
        $per_page = $request->per_page ?? 50;
        $rides = Ride::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Ride retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$rides),200);

    }
}

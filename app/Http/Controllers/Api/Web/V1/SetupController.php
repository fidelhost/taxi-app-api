<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\PricingParameter;
use App\Models\PricingParameterType;
use App\Models\VehicleTypeCategory;
use App\Models\VehicleName;
use App\Models\PricingCardSlot;
use App\Models\VehicleModel;
use App\Models\VehicleType;
use App\Models\PricingCard;
use App\Models\ServiceType;
use App\Models\VehicleMake;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;


use Validator;

class SetupController extends Controller
{


    public function getDocument(Request $request)
    {
        if($request->id){
            $area = Document::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Document  retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$area),200);
        }
        if($request->show_all == 'all'){
            $area = Document::all();
            $msg = ['status' => 'success', 'message' => __('Document retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$area),200);
        }
        $per_page = $request->per_page ?? 50;
        $areas = Document::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Document retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$areas),200);

    }

    public function createDocument(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:documents',
        ]);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $doc = new Document();
        $doc->name = $request->name;
        $doc->required = $request->required;
        $doc->mendatory = $request->mendatory;
        $doc->expired = $request->expired;
        $doc->save();
        $msg = ['status' => 'success', 'message' => __('Document create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updateDocument(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:documents,name,'.$request->name.',name',
        ]);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $doc = Document::where('id',$request->id)
                        ->update([
                            'name' => $request->name,
                            'required' => $request->required,
                            'mendatory' => $request->mendatory,
                            'expired' => $request->expired
                        ]);
        if ($doc) {
            $msg = ['status' => 'success', 'message' => 'Document update successfully', 'success' => true];
            return response()->json(Helper::api_output($msg,[]),200);
        }
    }

    public function getPricingParameter(Request $request)
    {
        $where = [];
        // if($request->get_type == 'approval') {
        //     $where['is_approve'] = false;
        // }elseif ($request->get_type == 'rejected') {
        //     $where['is_reject'] = true;
        // }
        $per_page = $request->per_page ?? 10;

        $parameters = PricingParameter::where($where)->with('parameter_type')->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Pricing parameter retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$parameters),200);
    }

    public function createPricingParameter(Request $request)
    {
        $parameter = new PricingParameter();
        $parameter->name = $request->name;
        $parameter->show_name = $request->show_name;
        $parameter->sequence = $request->sequence;
        $parameter->pricing_type = $request->pricing_type;
        $parameter->pricing_parameter_type_id = $request->parameter_type;
        $parameter->save();
        $msg = ['status' => 'success', 'message' => __('Parameter create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function getPricingParameterType(Request $request)
    {
        $where = [];
        // if($request->get_type == 'approval') {
        //     $where['is_approve'] = false;
        // }elseif ($request->get_type == 'rejected') {
        //     $where['is_reject'] = true;
        // }
        $per_page = $request->per_page ?? 10;
        $parameters = PricingParameterType::where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Pricing parameter type retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$parameters),200);
    }

    public function createPricingParameterType(Request $request)
    {
        $parameter = new PricingParameterType();
        $parameter->name = $request->name;
        $parameter->is_active = $request->is_active;
        // $parameter->description = $request->description;
        $parameter->save();
        $msg = ['status' => 'success', 'message' => __('Parameter type create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }


    public function getVehicleType(Request $request)
    {
        $where = [];
        // if($request->get_type == 'approval') {
        //     $where['is_approve'] = false;
        // }elseif ($request->get_type == 'rejected') {
        //     $where['is_reject'] = true;
        // }
        if($request->id){
            $type = VehicleType::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$type),200);
        }
        if($request->show_all == 'all'){
            $types = VehicleType::where($where)->get();
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }
        $per_page = $request->per_page ?? 10;
        $types = VehicleType::where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$types),200);
    }

    public function createVehicleType(Request $request)
    {
        // return response()->json($request->all());
        $type = new VehicleType();
        $type->name = $request->name;
        $type->rank = $request->rank;
        $type->sequence = $request->sequence;
        $type->description = $request->description;
        if ($request->avaiable_ride=='true') {
             $type->avaiable_ride = true;
        }
        if ($request->avaiable_pool=='true') {
             $type->avaiable_pool = true;
        }
        if ($request->avaiable_later=='true') {
             $type->avaiable_later = true;
        }
        $type->type_category_id = $request->type_category_id;
        if($request->hasFile('image')){
            $image = $request->image;
            $fileName = time().'_'. uniqid() .'.'. $image->getClientOriginalExtension();
            Storage::putFileAs('public/images', $image, $fileName);
            $type->selected_image = 'storage/images/' . $fileName;
            $type->save();
        }
        if($request->hasFile('mapimage')){
            $mapimage = $request->mapimage;
            $mfileName = time().'_'. uniqid() .'.'. $mapimage->getClientOriginalExtension();
            Storage::putFileAs('public/images', $mapimage, $mfileName);
            $type->map_image = 'storage/images/' . $mfileName;
            $type->save();
        }

        $type->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle type create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updateVehicleType(Request $request)
    {
        // return response()->json($request->all());
        $type = VehicleType::where('id',$request->id)->first();
        $type->name = $request->name;
        $type->rank = $request->rank;
        $type->sequence = $request->sequence;
        $type->description = $request->description;
        $type->avaiable_ride = $request->avaiable_ride;
        $type->avaiable_pool = $request->avaiable_pool;
        $type->avaiable_later = $request->avaiable_later;
        $type->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle type update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function getVehicleTypeCategory(Request $request)
    {
        $where = [];
        // if($request->get_type == 'approval') {
        //     $where['is_approve'] = false;
        // }elseif ($request->get_type == 'rejected') {
        //     $where['is_reject'] = true;
        // }
        if($request->id){
            $type = VehicleTypeCategory::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$type),200);
        }
        if($request->show_all == 'all'){
            $types = VehicleTypeCategory::where($where)->get();
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }
        $per_page = $request->per_page ?? 10;
        $types = VehicleTypeCategory::where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$types),200);
    }

    public function getVehicleName(Request $request)
    {
		$where = [];

        if($request->id){
			//$type = VehicleName::where('id',$request->id)->first();
            $type = DB::table('vehicle_names')->where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$type),200);
        }
        if($request->show_all == 'all'){
			//$types = VehicleName::where($where)->get();
            $types=DB::table('vehicle_names')->where($where)->get();
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }
        $per_page = $request->per_page ?? 10;
        $types=DB::table('vehicle_names')
			->join('vehicle_type_categories', 'vehicle_names.vehicle_type_category_id', '=', 'vehicle_type_categories.id')
			->where($where)
			->select('vehicle_names.*', 'vehicle_type_categories.name')
			->paginate($per_page);

        $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$types),200);
    }

    public function getVehicleTypeByCategory(Request $request)
    {
        if($request->id){
            $vtype = VehicleTypeCategory::where('id',$request->id)->first();
            $types= $vtype->vtype;
            $msg = ['status' => 'success', 'message' => __('Vehicle type retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }
    }

    public function createVehicleTypeCategory(Request $request)
    {
        // return response()->json($request->all());
        $type = new VehicleTypeCategory();
        $type->name = $request->name;
        $type->is_active = $request->is_active;
        // $type->description = $request->description;
        $type->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle type create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function createVehicleName(Request $request)
    {
        // return response()->json($request->all());
        // $type = new VehicleName();
        // $type->name = $request->name;
        // $type->vehicle_type_category_id = $request->type_category_id;

        // $type->save();


		DB::table('vehicle_names')->insert(
			[
				'vehicle_name' => $request->name,
				'vehicle_type_category_id' => $request->type_category_id,
				"created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
				"updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
			]
		);

        $msg = ['status' => 'success', 'message' => __('Vehicle name create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

	public function updateVehicleName(Request $request)
    {
        $affected = DB::table('vehicle_names')
            ->where('id', $request->id)
            ->update(
			[
				'vehicle_name' => $request->name,
				'vehicle_type_category_id' => $request->type_category_id,
				"updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
			]
		);

        $msg = ['status' => 'success', 'message' => __('Vehicle type update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

	public function deleteVehicleName(Request $request)
    {
        DB::table('vehicle_names')->where('id', '=', $request->id)->delete();

        $msg = ['status' => 'success', 'message' => __('Vehicle name deleted  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updateVehicleTypeCategory(Request $request)
    {
        // return response()->json($request->all());
        $type = VehicleTypeCategory::where('id',$request->id)->first();
        $type->name = $request->name;
        $type->vehicle_type_id = $request->type;
        $type->description = $request->description;
        $type->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle type update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function getVehicle(Request $request)
    {
        $where = [];
        if($request->id){
            $type = VehicleMake::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Vehicle  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$type),200);
        }
        if($request->show_all == 'all'){
            $types = VehicleMake::where($where)->get();
            $msg = ['status' => 'success', 'message' => __('Vehicle  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }

        $per_page = $request->per_page ?? 10;
        $types = VehicleMake::where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Vehicle  retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$types),200);
    }

    public function createVehicle(Request $request)
    {
        $type = new VehicleMake();
        $type->name = $request->name;
        $type->description = $request->description;
        $type->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle  create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updateVehicle(Request $request)
    {
        $type = VehicleMake::where('id',$request->id)->first();
        $type->name = $request->name;
        $type->description = $request->description;
        $type->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle  update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function getVehicleModel(Request $request)
    {
        $where = [];
        if($request->id){
            $type = VehicleModel::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Vehicle Model retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$type),200);
        }
        if($request->show_all == 'all'){
            $types = VehicleModel::where($where)->get();
            $msg = ['status' => 'success', 'message' => __('Vehicle  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }

        $per_page = $request->per_page ?? 10;
        $types = VehicleModel::where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Vehicle Model  retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$types),200);
    }

    public function createVehicleModel(Request $request)
    {
        $model = new VehicleModel();
        $model->vehicle_type_id = $request->type;
        $model->vehicle_make_id = $request->vehicle;
        $model->vehicle_model = $request->model;
        $model->seat = $request->seat;
        $model->description = $request->description;
        $model->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle Model  create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updateVehicleModel(Request $request)
    {
        $model = VehicleModel::where('id',$request->id)->first();
        $model->vehicle_type_id = $request->type;
        $model->vehicle_make_id = $request->vehicle;
        $model->vehicle_model = $request->model;
        $model->seat = $request->seat;
        $model->description = $request->description;
        $model->save();
        $msg = ['status' => 'success', 'message' => __('Vehicle Model  update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function action(Request $request)
    {
        $data = [];
        $msg = ['status' => 'success', 'message' => __('nothing'), 'success' => true];

        if($request->action_type == 'approve'){
            $data['is_approve'] = true;
            $data['is_reject'] = false;
            $msg = ['status' => 'success', 'message' => __('Driver approved  successfully'), 'success' => true];
        }else if($request->action_type == 'reject'){
            $data['is_reject'] = true;
            $data['is_approve'] = false;
            $msg = ['status' => 'success', 'message' => __('Driver rejected  successfully'), 'success' => true];
        }else if($request->action_type == 'ban'){
            $data['is_ban'] = true;
            $msg = ['status' => 'success', 'message' => __('Driver ban  successfully'), 'success' => true];
        }else if($request->action_type == 'unban'){
            $data['is_ban'] = false;
            $msg = ['status' => 'success', 'message' => __('Driver unban  successfully'), 'success' => true];
        }
        $diver = User::where('id',$request->id)->update($data);

        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function getPriceCardSlot(Request $request)
    {
        $slots = PricingCardSlot::where('pricing_card_id',$request->id)->get();

        foreach ($slots as $key => $slot) {
            $slot->week_days = json_decode($slot->week_days);
        }

        $msg = ['status' => 'success', 'message' => __('Slot  retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$slots),200);

    }

    public function getPriceCard(Request $request)
    {
        if($request->show_all == 'all'){
            $types = PricingCard::where($where)->get();
            $msg = ['status' => 'success', 'message' => __('Vehicle  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }

        if($request->id){
            $types = PricingCard::where('id',$request->id)->first();
            $types->payment_method = $types->payment_method ? json_decode($types->payment_method) : [];
            $msg = ['status' => 'success', 'message' => __('Price card  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }

        $per_page = $request->per_page ?? 10;
        $types = PricingCard::paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Price card  retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$types),200);
    }

    public function createPricingCard(Request $request)
    {

        $card = new PricingCard();
        $card->name = $request->name;
        $card->area_id = $request->area_id;
        $card->service_id = $request->service_id;
        $card->fare_id = $request->fare_id;
        $card->package_id = $request->package_id;
        $card->vehicle_type_id = $request->vehicle_type_id;
        $card->driver_commission_type = $request->driver_commission_type;
        $card->driver_flat_type = $request->driver_flat_type;
        $card->driver_commission_value = $request->driver_commission_value;
        $card->driver_commission_value_per =  $request->driver_commission_value_per;
        $card->hotel_commission_type = $request->hotel_commission_type;
        $card->hotel_flat_type = $request->hotel_flat_type;
        $card->hotel_commission_value = $request->hotel_commission_value;
        $card->hotel_commission_value_per = $request->hotel_commission_value_per;
        $card->taxi_c_commission_type = $request->taxi_c_commission_type;
        $card->taxi_c_commission_flat_type = $request->taxi_c_commission_flat_type;
        $card->taxi_c_commission_value = $request->taxi_c_commission_value;
        $card->taxi_c_commission_value_per = $request->taxi_c_commission_value_per;
        $card->is_surge_charge = true;
        $card->surge_charge_type = $request->surge_charge_type;
        $card->surge_charge_value = $request->surge_charge_value;
        $card->payment_method = json_encode($request->payment_method);

        $card->bill_type = $request->bill_type;
        $card->max_distance = $request->max_distance;
        $card->u_min_wallet_balance = $request->u_min_wallet_balance;
        $card->extra_seat_charge =  $request->extra_seat_charge;
        $card->max_bill = $request->max_bill;

        $card->is_insurance = $request->is_insurance;
        $card->insurance_type = $request->insurance_type;
        $card->insurance_value = $request->insurance_value;

        $card->save();



        foreach ($request->slots as $key => $slot) {
            $slot = (object) $slot;

            PricingCardSlot::forceCreate([
                'pricing_card_id' => $card->id,
                'week_days' => json_encode($slot->week_days),
                'parameter_name' => $slot->parameter_name,
                'start_time' => $slot->parameter_name,
                'end_time' => $slot->end_time,
                'time_type' => $slot->time_type,
                'charge' => $slot->charge,
                'charge_type' => $slot->charge_type
            ]);
        }


        $msg = ['status' => 'success', 'message' => __('Pricing card create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updatePricingCard(Request $request)
    {
        // return response()->json($request->all());
        $card =  PricingCard::where('id',$request->id)->first();
        // return response()->json($card);
        $card->name = $request->name;
        $card->area_id = $request->area_id;
        $card->service_id = $request->service_id;
        $card->fare_id = $request->fare_id;
        $card->package_id = $request->package_id;
        $card->vehicle_type_id = $request->vehicle_type_id;
        $card->driver_commission_type = $request->driver_commission_type;
        $card->driver_flat_type = $request->driver_flat_type;
        $card->driver_commission_value = $request->driver_commission_value;
        $card->driver_commission_value_per =  $request->driver_commission_value_per;
        $card->hotel_commission_type = $request->hotel_commission_type;
        $card->hotel_flat_type = $request->hotel_flat_type;
        $card->hotel_commission_value = $request->hotel_commission_value;
        $card->hotel_commission_value_per = $request->hotel_commission_value_per;
        $card->taxi_c_commission_type = $request->taxi_c_commission_type;
        $card->taxi_c_commission_flat_type = $request->taxi_c_commission_flat_type;
        $card->taxi_c_commission_value = $request->taxi_c_commission_value;
        $card->taxi_c_commission_value_per = $request->taxi_c_commission_value_per;
        $card->is_surge_charge = true;
        $card->surge_charge_type = $request->surge_charge_type;
        $card->surge_charge_value = $request->surge_charge_value;
        $card->payment_method = $request->payment_method;

        $card->is_insurance = $request->is_insurance;
        $card->insurance_type = $request->insurance_type;
        $card->insurance_value = $request->insurance_value;

        // $card->bill_type = $request->bill_type;
        // $card->max_distance = $request->max_distance;
        // $card->u_min_wallet_balance = $request->u_min_wallet_balance;
        // $card->extra_seat_charge =  $request->extra_seat_charge;
        // $card->max_bill = $request->max_bill;

        $card->save();

        PricingCardSlot::where('pricing_card_id',$card->id)->delete();

        foreach ($request->slots as $key => $slot) {
            $slot = (object) $slot;

            PricingCardSlot::forceCreate([
                'pricing_card_id' => $card->id,
                'week_days' => json_encode($slot->week_days),
                'parameter_name' => $slot->parameter_name,
                'start_time' => $slot->parameter_name,
                'end_time' => $slot->end_time,
                'time_type' => $slot->time_type,
                'charge' => $slot->charge,
                'charge_type' => $slot->charge_type
            ]);
        }

        $msg = ['status' => 'success', 'message' => __('Pricing card update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }


    // service type

    public function getServiceType(Request $request)
    {
        $where = [];

        if($request->show_all == 'all'){
            $types = ServiceType::where($where)->get();
            $msg = ['status' => 'success', 'message' => __('service  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$types),200);
        }

        if($request->id ){
            $service = ServiceType::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('service type  retrieve  successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$service),200);
        }

        $per_page = $request->per_page ?? 10;
        $service = ServiceType::where($where)->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Service type retrieve  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$service),200);
    }

    public function createServiceType(Request $request)
    {
        $service = new ServiceType();
        $service->type = $request->type;
        $service->name = $request->name;
        $service->color = $request->color;
        $service->save();
        $msg = ['status' => 'success', 'message' => __('Service type create  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }

    public function updateServiceType(Request $request)
    {
        $service = ServiceType::where('id',$request->id)->first();
        $service->type = $request->type;
        $service->name = $request->name;
        $service->color = $request->color;
        $service->save();
        $msg = ['status' => 'success', 'message' => __('Service type update  successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);
    }


    public function getBillFields(Request $request)
    {
        $fields=PricingParameter::where('pricing_type', $request->val)->get('show_name');
        $msg = ['status' => 'success', 'message' => __('Bill type fields retirived'), 'success' => true];
        return response()->json(Helper::api_output($msg,$fields),200);
    }
}

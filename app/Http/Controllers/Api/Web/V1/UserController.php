<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\RoleUser;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use DB;

class UserController extends Controller
{
    public function getPermissions()
    {
        $roleID = \DB::table('role_user')->where('user_id',the_auth_id())->first();
        if($roleID){
            $permIDs = \DB::table('role_permission')->where('role_id',$roleID->role_id)->pluck('id')->toArray();
            $permissions = Permission::whereBetween('id',$permIDs)->pluck('slug')->toArray();
            $msg = ['status' => 'success', 'message' => __('User permission retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$permissions),200);
        }
    }

    public function getAllId()
    {
        $users = DB::table('users')->get(['id','name','phone','email']);
        $msg = ['status' => 'success', 'message' => __('Users retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$users),200);

    }

    public function get(Request $request)
    {
        $per_page = $request->per_page ?? 50;
        $users = User::with('role')->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Users retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$users),200);
    }

    public function createUser(Request $request)
    {
        $validate = [];
        if($request->email){
            // $validateData['email'] = ['required'];
            $usernameData = [
                'email' => 'required|unique:users',
                'phone' => 'unique:users'
            ];
        }else{
            $usernameData = [
                'phone' => 'required|unique:users',
                'email' => 'unique:users',
            ];
        }
        $validate = array_merge($validate,$usernameData);
        $validateData = [
            'password' => 'required|min:8'
        ];
        $validateData = array_merge($validate,$validateData);
        $validator = Validator::make($request->all(),$validateData);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $user = User::forceCreate([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'type' => 'staff'
        ])->id;
        if($request->role_id){
            RoleUser::forceCreate([
                'user_id' => $user,
                'role_id' => $request->role_id
            ]);
        }


        $msg = ['status' => 'error', 'message' => __('User create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$user),200);
    }

    public function updateUser(Request $request)
    {

        $validate = [];
        if($request->email){
            // $validateData['email'] = ['required'];
            $usernameData = [
                'email' => 'required|unique:users,email,'.$request->email.',email',
                'phone' => 'required|unique:users,phone,'.$request->phone.',phone',
            ];
        }else{
            $usernameData = [
                'phone' => 'required|unique:users,phone,'.$request->phone.',phone',
                'email' => 'required|unique:users,email,'.$request->email.',email',
            ];
        }
        $validate = array_merge($validate,$usernameData);
        $validateData = [
            'password' => 'required|min:8'
        ];
        $validateData = array_merge($validate,$validateData);
        $validator = Validator::make($request->all(),$validateData);

        if ($validator->fails()) {
            $msg = ['status' => 'error', 'message' => $validator->errors()->first(), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        $user = User::where('id',$request->id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'type' => 'staff'
        ]);
        if($request->role_id){
            RoleUser::where('user_id',$request->id)->update([
                'user_id' => $request->id,
                'role_id' => $request->role_id
            ]);
        }


        $msg = ['status' => 'error', 'message' => __('User update successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$user),200);
    }
}

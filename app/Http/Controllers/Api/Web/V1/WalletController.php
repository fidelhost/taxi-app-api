<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\WalletTransaction;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use File;

class WalletController extends Controller
{

    public function get(Request $request)
    {
        if ($request->id) {
            $wallet = WalletTransaction::where('id',$request->id)->first();
            $msg = ['status' => 'success', 'message' => __('Wallet retrive successfully'), 'success' => true];
            return response()->json(Helper::api_output($msg,$wallet),200);
        }
        $per_page = $request->per_page ?? 50;
        $wallet = WalletTransaction::with(['senderInfo','receiverInfo'])->paginate($per_page);
        $msg = ['status' => 'success', 'message' => __('Language retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,$wallet),200);
    }

    public function addTransaction(Request $request)
    {

        if(!User::where('id',$request->user)->exists()){
            $msg = ['status' => 'success', 'message' => __('User not valid'), 'success' => false];
            return response()->json(Helper::api_output($msg,[]),200);
        }

        WalletTransaction::forceCreate([
            'user_id' => $request->user,
            'sender' => 1,
            'amount' => $request->amount,
            'type'  => 'recharge',
            'method' => 'admin'
        ]);

        $msg = ['status' => 'success', 'message' => __('Transaction create successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,[]),200);

        // $trans = WalletTransaction::where('user_id',$request->reciever)
        //                     ->where('wallet_merge',false)
        //                     ->get();
        //
        // $wallet = Wallet::where('user_id',$request->reciever)->first();
        // $wallet->amount = $wallet->amount + $trans->amount;
        // $wallet->save();
    }


}

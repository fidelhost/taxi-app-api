<?php

namespace App\Http\Controllers\Api\Web\V1;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Meta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class WebsiteSettingController extends Controller
{
    public function create(Request $request)
    {
       $data = $request->except(['_token','logo']);
       // Storage::disk('local')->put('file.txt', 'Contents');
       if($request->hasFile('logo')){
           $logo = image_upload($request->file('logo'),'images','site-logo');
           Meta::updateOrCreate(
                ['purpose'=>'website_setting','key'=>'logo'],
                [
                   'purpose' => 'website_setting',
                   'key' => 'logo',
                   'value' => json_encode(['path' => 'images','file'=>$logo])
                ]
            );
       }


       foreach ($data as $key => $value) {
           Meta::updateOrCreate(
                ['purpose'=>'website_setting','key'=>$key],
                [
                   'purpose' => 'website_setting',
                   'key' => $key,
                   'value' => $value
                ]
            );
       }
       $msg = ['status' => 'success', 'message' => __( 'Website setting set successfully'), 'success' => true];
       return response()->json(Helper::api_output($msg,$request->hasFile('logo')),200);
    }

    public function get()
    {
        $msg = ['status' => 'success', 'message' => __( 'Website setting retrive successfully'), 'success' => true];
        return response()->json(Helper::api_output($msg,setting_website()),200);
    }

}

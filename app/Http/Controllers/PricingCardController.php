<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\PricingCardStoreRequest;
use App\Http\Resources\CountryResource;
use App\Http\Resources\PricingCardResource;
use App\Models\PricingCard;
use App\Models\PricingCardSlot;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class PricingCardController extends Controller
{
    public $pricingCard;

    public function __construct()
    {
        $this->pricingCard = new PricingCard();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return PricingCardResource::collection($this->pricingCard->getPricingCards($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return CountryResource
     */
    public function show($id)
    {
        return new PricingCardResource(PricingCard::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PricingCardStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $pricingCard = PricingCard::create($input);
            $this->save($request, $pricingCard);
            DB::commit();

            $result['pricing_card_id'] = $pricingCard->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function save($request, $pricingCard){

        $pricingCardSlots = $request->pricing_card_slots;
        if($pricingCardSlots){
            foreach ($pricingCardSlots as $pricingCardSlot) {
                $pricingCardSlot['pricing_card_id'] = $pricingCard->id;
                PricingCardSlot::create($pricingCardSlot);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(PricingCardStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $pricingCard = PricingCard::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $pricingCard->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $pricingCard = PricingCard::findOrFail($id);
            $iconPath = 'storage/'.$pricingCard->icon;
            if(File::exists($iconPath)) {
                File::delete($iconPath);
            }
            $pricingCard->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PromoCodeStoreRequest;
use App\Http\Resources\PromoCodeResource;
use App\Models\PromoCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Symfony\Component\HttpFoundation\Response;
use Validator;

class PromoCodeController extends Controller
{
    public $promoCode;

    public function __construct()
    {
        $this->promoCode = new PromoCode();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return PromoCodeResource::collection($this->promoCode->getPromoCodes($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return PromoCodeResource
     */
    public function show($id)
    {
        return new PromoCodeResource(PromoCode::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PromoCodeStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $promoCode = PromoCode::create($input);

            $this->save($request, $promoCode);
            DB::commit();

            $result['promo_code_id'] = $promoCode->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function save($request, $promoCode)
    {
        $pricingCardsData = [];
        $pricingCards = $request->pricing_card_id;

        if ($pricingCards) {
            foreach ($pricingCards as $key => $pricingCardId) {
                $pricingCardsData[$pricingCardId]['id'] = (string)Str::uuid();
            }
        }

        $promoCode->pricingCard()->attach($pricingCardsData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(promoCodeStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $promoCode = PromoCode::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $promoCode->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $promoCode = PromoCode::findOrFail($id);
            $promoCode->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

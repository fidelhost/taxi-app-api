<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServiceAreaStoreRequest;
use App\Http\Resources\CountryResource;
use App\Http\Resources\ServiceAreaResource;
use App\Models\ServiceArea;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class ServiceAreaController extends Controller
{
    public $serviceArea;

    public function __construct()
    {
        $this->serviceArea = new ServiceArea();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return ServiceAreaResource::collection($this->serviceArea->getServiceAreas($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return CountryResource
     */
    public function show($id)
    {
        return new ServiceAreaResource(ServiceArea::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ServiceAreaStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $serviceArea = ServiceArea::create($input);
            $this->save($request, $serviceArea);
            DB::commit();

            $result['service_area_id'] = $serviceArea->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function save($request, $serviceArea){

        $vehicleTypesData = [];
        $vehicleTypes = $request->vehicle_type_id;
        if($vehicleTypes){
            foreach ($vehicleTypes as $key => $vehicleTypeId) {
                $vehicleTypesData[$vehicleTypeId]['id'] = (string)Str::uuid();
            }
        }
        $serviceArea->vehicleType()->attach($vehicleTypesData);

        $normalServiceData = [];
        $normalServiceVehicleTypes = $request->normal_service_vehicle_type_id;
        if($normalServiceVehicleTypes){
            foreach ($normalServiceVehicleTypes as $key => $normalServiceVehicleTypeId) {
                $normalServiceData[$normalServiceVehicleTypeId]['id'] = (string)Str::uuid();
            }
        }
        $serviceArea->normalServiceVehicleType()->attach($normalServiceData);

        $outstationServiceData = [];
        $outstationServiceVehicleTypes = $request->outstation_service_vehicle_type_id;
        if($outstationServiceVehicleTypes){
            foreach ($outstationServiceVehicleTypes as $key => $outstationServiceVehicleTypeId) {
                $outstationServiceData[$outstationServiceVehicleTypeId]['id'] = (string)Str::uuid();
            }
        }
        $serviceArea->outstationServiceVehicleType()->attach($outstationServiceData);

//        $rentalServiceData = [];
//        $rentalServiceVehicleTypes = $request->rental_service_vehicle_type_id;
//        if($rentalServiceVehicleTypes){
//            foreach ($rentalServiceVehicleTypes as $key => $rentalServiceVehicleTypeId) {
//                $rentalServiceData[$rentalServiceVehicleTypeId]['id'] = (string)Str::uuid();
//            }
//        }
//        $serviceArea->rentalServiceVehicleType()->attach($rentalServiceData);

        $driverDocsData = [];
        $driverDocs = $request->driver_doc_id;
        if($driverDocs){
            foreach ($driverDocs as $key => $driverDocId) {
                $driverDocsData[$driverDocId]['id'] = (string)Str::uuid();
            }
        }
        $serviceArea->driverDocs()->attach($driverDocsData);

        $vehicleDocsData = [];
        $vehicleDocs = $request->vehicle_doc_id;
        if($vehicleDocs){
            foreach ($vehicleDocs as $key => $vehicleDocId) {
                $vehicleDocsData[$vehicleDocId]['id'] = (string)Str::uuid();
            }
        }
        $serviceArea->vehicleDocs()->attach($vehicleDocsData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(ServiceAreaStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $serviceArea = ServiceArea::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $serviceArea->update($input);
            $this->save($request, $serviceArea);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $serviceArea = ServiceArea::findOrFail($id);
            $serviceArea->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

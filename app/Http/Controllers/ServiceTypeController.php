<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\ServiceTypeStoreRequest;
use App\Http\Resources\CountryResource;
use App\Http\Resources\ServiceTypeResource;
use App\Models\ServiceType;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Validator;

class ServiceTypeController extends Controller
{
    public $serviceType;

    public function __construct()
    {
        $this->serviceType = new ServiceType();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return ServiceTypeResource::collection($this->serviceType->getServiceTypes($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return CountryResource
     */
    public function show($id)
    {
        return new ServiceTypeResource(ServiceType::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ServiceTypeStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $serviceType = ServiceType::create($input);
            if($request->hasFile('icon')){
                $image_path = 'images/service-type';
                $image_name =  Helper::image_upload($request->file('icon'),$image_path, 'icon-'.$serviceType->id);
                $serviceType->icon = $image_path.'/'.$image_name;
                $serviceType->save();
            }
            DB::commit();

            $result['service_type_id'] = $serviceType->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(ServiceTypeStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $serviceType = ServiceType::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $serviceType->update($input);

            if($request->hasFile('icon')){

                $iconPath = 'storage/'.$serviceType->icon;
                if(File::exists($iconPath)) {
                    File::delete($iconPath);
                }

                $image_path = 'images/service-type';
                $image_name =  Helper::image_upload($request->file('icon'),$image_path, 'icon-'.$serviceType->id);
                $serviceType->icon = $image_path.'/'.$image_name;
                $serviceType->save();
            }
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $serviceType = ServiceType::findOrFail($id);
            $iconPath = 'storage/'.$serviceType->icon;
            if(File::exists($iconPath)) {
                File::delete($iconPath);
            }
            $serviceType->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

<?php
namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserStoreRequest;
use App\Http\Resources\UserResource;
use App\Models\Meta;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = new User;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return UserResource::collection($this->user->getUsers($request)->latest()->get());
    }

    /*
     * login api
     *
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $email = $request->get('email');
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        }else{
            $field = 'phone';
        }
        if (Auth::attempt([$field => $email, 'password' => $request->password, 'is_active' => 1])) {
            $user = Auth::user();
            $user->last_login_at = Carbon::now();
            $user->last_login_ip_address = $request->ip();
            $user->save();

            $result['message'] = 'Successfully Logged in';
            $result['token'] = $user->createToken('TaxiApp')->accessToken;
            $result['user_id'] = $user->id;
            $result['name'] = $user->name;
            return new JsonResponse($result, Response::HTTP_OK);
        }

        $userInfo = User::where('email', $email)->first();

        if (!empty($userInfo->email)) {
            if ($userInfo->is_active == 1) {
                $result['message'] = 'You are registered but given password is wrong';
                $result['details'] = [
                    "email" => "Right Email",
                    "password" => "Wrong Password",
                ];
            } else {
                $result['message'] = 'Your account is deactivated. Please, contact with admin. Thank you.';
            }

        } else {
            $result['message'] = 'You are not registered';
            $result['details'] = [
                "email" => "Wrong Email"
            ];
        }
        $result['timestamp'] = Carbon::now();
        return new JsonResponse($result, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Register api
     *
     * @return GenericUserResource
     */
    public function store(UserStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $input['ip_address'] = $request->ip();


            $user = User::create($input);

            if($request->hasFile('image')){
                $image_path = 'images/users';
                $image_name =  Helper::image_upload($request->file('image'),$image_path,$user->id.'-profile');
                $user->image = $image_path.'/'.$image_name;
                $user->save();
            }

            $result['message'] = 'You are successfully registered.';
            $result['token'] = $user->createToken('Apical')->accessToken;
            $result['user_id'] = $user->id;
            $result['name'] = $user->name;
            DB::commit();

            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function show($id){
        return new UserResource(User::findOrFail($id));
    }

    public function userList(Request $request)
    {
        return UserResource::collection($this->user->getUserList($request));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return JsonResponse
     */
    public function update(UserStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try{
            $user = User::findOrFail($id);
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $input['updated_by'] = Auth::id();
            $user->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        }catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $user = User::findOrFail($id);
            $user->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        }catch(\Exception $e){
            DB::rollBack();
            throw $e;
        }
    }
}

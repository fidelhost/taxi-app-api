<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleMakeStoreRequest;
use App\Http\Resources\VehicleMakeResource;
use App\Models\VehicleMake;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Symfony\Component\HttpFoundation\Response;
use Validator;

class VehicleMakeController extends Controller
{
    public $vehicleMake;

    public function __construct()
    {
        $this->vehicleMake = new VehicleMake();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return VehicleMakeResource::collection($this->vehicleMake->getVehicleMakes($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return VehicleMakeResource
     */
    public function show($id)
    {
        return new VehicleMakeResource(VehicleMake::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleMakeStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $vehicleMake = VehicleMake::create($input);
            DB::commit();

            $result['vehicle_make_id'] = $vehicleMake->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(VehicleMakeStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $vehicleMake = VehicleMake::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $vehicleMake->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $vehicleMake = VehicleMake::findOrFail($id);
            $vehicleMake->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleNameStoreRequest;
use App\Http\Resources\VehicleNameResource;
use App\Models\VehicleName;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Symfony\Component\HttpFoundation\Response;
use Validator;

class VehicleNameController extends Controller
{
    public $vehicleName;

    public function __construct()
    {
        $this->vehicleName = new VehicleName();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return VehicleNameResource::collection($this->vehicleName->getVehicleNames($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return VehicleNameResource
     */
    public function show($id)
    {
        return new VehicleNameResource(VehicleName::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleNameStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $vehicleName = VehicleName::create($input);

            if($request->hasFile('image')){
                $image_path = 'images/vehicle-names';
                $image_name =  Helper::image_upload($request->file('image'),$image_path,$vehicleName->id);
                $vehicleName->image = $image_path.'/'.$image_name;
                $vehicleName->save();
            }

            DB::commit();

            $result['vehicle_name_id'] = $vehicleName->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(VehicleNameStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $vehicleName = VehicleName::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $vehicleName->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $vehicleName = VehicleName::findOrFail($id);
            $vehicleName->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleTypeCategoryStoreRequest;
use App\Http\Resources\VehicleTypeCategoryResource;
use App\Models\VehicleTypeCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Symfony\Component\HttpFoundation\Response;
use Validator;

class VehicleTypeCategoryController extends Controller
{
    public $vehicleTypeCategory;

    public function __construct()
    {
        $this->vehicleTypeCategory = new VehicleTypeCategory();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return VehicleTypeCategoryResource::collection($this->vehicleTypeCategory->getVehicleTypeCategories($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return VehicleTypeCategoryResource
     */
    public function show($id)
    {
        return new VehicleTypeCategoryResource(VehicleTypeCategory::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleTypeCategoryStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $vehicleTypeCategory = VehicleTypeCategory::create($input);
            DB::commit();

            $result['vehicle_type_category_id'] = $vehicleTypeCategory->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(VehicleTypeCategoryStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $vehicleTypeCategory = VehicleTypeCategory::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $vehicleTypeCategory->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $vehicleTypeCategory = VehicleTypeCategory::findOrFail($id);
            $vehicleTypeCategory->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

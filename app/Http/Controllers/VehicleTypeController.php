<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleTypeStoreRequest;
use App\Http\Resources\VehicleTypeResource;
use App\Models\VehicleType;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Symfony\Component\HttpFoundation\Response;
use Validator;

class VehicleTypeController extends Controller
{
    public $vehicleType;

    public function __construct()
    {
        $this->vehicleType = new VehicleType();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        return VehicleTypeResource::collection($this->vehicleType->getVehicleTypes($request));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return VehicleTypeResource
     */
    public function show($id)
    {
        return new VehicleTypeResource(VehicleType::findOrFail($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleTypeStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $input = $request->all();
            $input['created_by'] = Auth::id();
            $input['updated_by'] = Auth::id();
            $vehicleType = VehicleType::create($input);

            if($request->hasFile('image')){
                $image_path = 'images/vehicle-types/'.$vehicleType->id;
                $image_name =  Helper::image_upload($request->file('image'),$image_path,'image');
                $vehicleType->image = $image_path.'/'.$image_name;
                $vehicleType->save();
            }
            if($request->hasFile('selected_image')){
                $image_path = 'images/vehicle-types/'.$vehicleType->id;
                $image_name =  Helper::image_upload($request->file('selected_image'),$image_path,'selected-image');
                $vehicleType->selected_image = $image_path.'/'.$image_name;
                $vehicleType->save();
            }
            if($request->hasFile('image')){
                $image_path = 'images/vehicle-types/'.$vehicleType->id;
                $image_name =  Helper::image_upload($request->file('map_image'),$image_path,'map-image');
                $vehicleType->map_image = $image_path.'/'.$image_name;
                $vehicleType->save();
            }
            DB::commit();

            $result['vehicle_type_id'] = $vehicleType->id;
            $result['message'] = config('settings.message.saved');
            return new JsonResponse($result, Response::HTTP_CREATED);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(VehicleTypeStoreRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $vehicleType = VehicleType::findOrFail($id);
            $input = $request->all();
            $input['updated_by'] = Auth::id();
            $vehicleType->update($input);
            DB::commit();

            $result['message'] = config('settings.message.updated');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $vehicleType = VehicleType::findOrFail($id);
            $vehicleType->delete();
            DB::commit();

            $result['message'] = config('settings.message.deleted');
            return new JsonResponse($result, Response::HTTP_OK);
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}

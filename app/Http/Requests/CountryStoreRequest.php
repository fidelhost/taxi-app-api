<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class CountryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $result['message'] = config('settings.message.validation_fail');
        $result['timestamp'] = Carbon::now();
        $result['details'] = $validator->errors();

        throw new HttpResponseException(response()->json($result, Response::HTTP_BAD_REQUEST));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:countries|max:255,name,'.$this->uuid,
            'code' => 'required|unique:countries|max:20,code,'.$this->uuid,
            'currency' => 'required',
            'currency_code' => 'required',
            'min_phone_digit' => 'required',
            'max_phone_digit' => 'required',
            'online_transaction_code' => 'required',
            'sequence' => 'required',
            'is_active' => 'required|boolean|',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}

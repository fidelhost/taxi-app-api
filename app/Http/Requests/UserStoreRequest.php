<?php

namespace App\Http\Requests;

use App\Rules\Password;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        $result['message'] = config('settings.message.validation_fail');
        $result['timestamp'] = Carbon::now();
        $result['details'] = $validator->errors();

        throw new HttpResponseException(response()->json($result, Response::HTTP_BAD_REQUEST));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users|max:255,email,'.$this->uuid,
            'phone' => 'required|unique:users|max:20,phone,'.$this->uuid,
            'nid_number' => 'unique:users|max:25,nid_number,'.$this->uuid,
            'first_name' => 'required|max:80',
            'type' => 'required',
            'is_active' => 'required|boolean|',
            'is_accepted_tc' => 'required|boolean|',
            'password' => [
                'required',
                'min:6',
                'confirmed'
            ]
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CountryResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'currency' => $this->currency,
            'currency_code' => $this->currency_code,
            'default_lang' => $this->default_lang,
            'distance_unit' => $this->distance_unit,
            'min_phone_digit' => $this->min_phone_digit,
            'max_phone_digit'=> $this->max_phone_digit,
            'online_transaction_code'=> $this->online_transaction_code,
            'sequence'=> $this->sequence,
        ];
    }
}

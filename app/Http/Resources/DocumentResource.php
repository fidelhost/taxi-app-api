<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mandatory' => $this->mandatory,
            'expired' => $this->expired,
            'required' => $this->required,
            'type' => $this->type,
            'is_active'=> $this->is_active,
        ];
    }
}

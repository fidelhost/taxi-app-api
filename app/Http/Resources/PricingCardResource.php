<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PricingCardResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'package_id' => $this->package_id,
            'is_applicable_all_type_vehicles' => $this->is_applicable_all_type_vehicles,
            'bill_type' => $this->bill_type,
            'u_min_wallet_balance' => $this->u_min_wallet_balance,
            'extra_seat_charge' => $this->extra_seat_charge,
            'max_distance' => $this->max_distance,
            'max_bill' => $this->max_bill,
            'driver_commission_type' => $this->driver_commission_type,
            'driver_flat_type' => $this->driver_flat_type,
            'driver_commission_value' => $this->driver_commission_value,
            'driver_commission_value_per' => $this->driver_commission_value_per,
            'taxi_c_commission_type' => $this->taxi_c_commission_type,
            'taxi_c_commission_flat_type' => $this->taxi_c_commission_flat_type,
            'taxi_c_commission_value' => $this->taxi_c_commission_value,
            'taxi_c_commission_value_per' => $this->taxi_c_commission_value_per,
            'hotel_commission_type' => $this->hotel_commission_type,
            'hotel_flat_type' => $this->hotel_flat_type,
            'hotel_commission_value' => $this->hotel_commission_value,
            'hotel_commission_value_per' => $this->hotel_commission_value_per,
            'is_surge_charge' => $this->is_surge_charge,
            'surge_charge_type' => $this->surge_charge_type,
            'surge_charge_value' => $this->surge_charge_value,
            'payment_method' => $this->payment_method,
            'is_insurance' => $this->is_insurance,
            'insurance_type' => $this->insurance_type,
            'insurance_value' => $this->insurance_value,
            'is_active'=> $this->is_active,
            'service_area' => $this->serviceArea,
            'service_type' => $this->serviceType,
            'vehicle_type' => $this->vehicleType,
            'fare_type' => $this->fareType,
            'pricing_card_slots' => $this->pricingCardSlots
        ];
    }
}

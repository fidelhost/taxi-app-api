<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PricingParameterResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parameter_type_id' => $this->parameter_type_id,
            'application_name' => $this->application_name,
            'sequence' => $this->sequence,
            'application_for' => $this->application_for,
            'is_active'=> $this->is_active,
        ];
    }
}

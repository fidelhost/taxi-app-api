<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PromoCodeResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'promo_codes_setup' => $this->promo_codes_setup,
            'promo_type' => $this->promo_type,
            'discount' => $this->discount,
            'description' => $this->description,
            'valid_for' => $this->valid_for,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'applicable_for' => $this->applicable_for,
            'promo_code_limit' => $this->promo_code_limit,
            'promo_code_limit_per_rider' => $this->promo_code_limit_per_rider,
            'promo_Percentage_max_discount' => $this->promo_Percentage_max_discount,
            'pricing_parameter_name' => $this->pricing_parameter_name,
            'corporate_id' => $this->corporate_id,
            'is_active'=> $this->is_active,
            'service_area'=> $this->serviceArea,
            'pricing_cards'=> $this->pricingCard,
        ];
    }
}

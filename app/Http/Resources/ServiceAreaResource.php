<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceAreaResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'country_id' => $this->country_id,
            'timezone' => $this->timezone,
            'polygon_area_of_map' => $this->polygon_area_of_map,
            'pool_service' => $this->pool_service,
            'position_of_pool_on_home_screen' => $this->position_of_pool_on_home_screen,
            'driver_min_wallet_balance' => $this->driver_min_wallet_balance,
            'driver_bill_period'=> $this->driver_bill_period,
            'driver_bill_start_time'=> $this->driver_bill_start_time,
            'driver_bill_start_day'=> $this->driver_bill_start_day,
            'is_airport'=> $this->is_airport,
            'is_auto_upgrate'=> $this->is_auto_upgrate,
            'country'=> $this->country,
            'vehicleType'=> $this->vehicleType,
            'normalServiceVehicleType'=> $this->normalServiceVehicleType,
            'outstationServiceVehicleType'=> $this->outstationServiceVehicleType,
//            'rentalServiceVehicleType'=> $this->rentalServiceVehicleType,
            'driverDocs'=> $this->driverDocs,
            'vehicleDocs'=> $this->vehicleDocs,
        ];
    }
}

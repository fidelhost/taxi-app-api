<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phone ' => $this->phone,
            'type' => $this->type,
            'is_active' => $this->is_active,
            'is_ban' => $this->is_ban,
            'is_approve' => $this->is_approve,
            'is_reject' => $this->is_reject
        ];
    }
}

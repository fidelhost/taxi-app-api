<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleModelResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'vehicle_type_id' => $this->vehicle_type_id,
            'vehicle_make_id' => $this->vehicle_make_id,
            'country_id' => $this->country_id,
            'seat' => $this->seat,
            'description' => $this->description,
            'is_active'=> $this->is_active,
        ];
    }
}

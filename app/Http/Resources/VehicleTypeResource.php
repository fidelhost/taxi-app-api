<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicleTypeResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'vehicle_name_id' => $this->vehicle_name_id,
            'type_category_id' => $this->type_category_id,
            'rank' => $this->rank,
            'sequence' => $this->sequence,
            'description' => $this->description,
            'image' => $this->image,
            'selected_image' => $this->selected_image,
            'map_image' => $this->map_image,
            'available_ride' => $this->available_ride,
            'available_later' => $this->available_later,
            'available_pool' => $this->available_pool,
            'is_active'=> $this->is_active,
        ];
    }
}

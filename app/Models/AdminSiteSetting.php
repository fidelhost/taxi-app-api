<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AdminSiteSetting extends Model
{
    protected $table = 'admin_site_settings';
}

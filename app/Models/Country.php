<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'name', 'code', 'currency', 'currency_code', 'default_lang',
        'distance_unit',  'min_phone_digit', 'max_phone_digit', 'online_transaction_code',
        'sequence', 'created_by', 'updated_by'
    ];

    public function getCountries($request){

        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function scopeOfSearch($query, $request){

        $search = $request->query('search');

        if (!empty($search)) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%');
            });
        }
        return $query;
    }
}

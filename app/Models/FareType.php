<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class FareType extends Model
{
    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'name', 'is_active', 'created_by', 'updated_by'
    ];
}

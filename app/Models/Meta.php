<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Meta extends Model
{
    protected $table = 'meta';
    protected $fillable = ['purpose','key','value'];
}

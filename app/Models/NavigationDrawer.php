<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class NavigationDrawer extends Model
{
    protected $table = 'navigation_drawers';
}

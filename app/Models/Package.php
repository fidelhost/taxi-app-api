<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use UsesUuid, SoftDeletes, HasFactory;

    protected $fillable = [
        'type', 'name', 'description', 'term_and_condition', 'is_active', 'created_by', 'updated_by'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PointCondition extends Model
{
    protected $table = 'point_conditions';
    protected $fillable = ['purpose','amount','point','per_point','per_point_price'];
}

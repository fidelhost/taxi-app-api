<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class PricingCard extends Model
{
    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'name', 'service_area_id', 'service_type_id', 'fare_type_id', 'vehicle_type_id', 'package_id', 'is_applicable_all_type_vehicles',
        'bill_type', 'u_min_wallet_balance', 'extra_seat_charge', 'max_distance', 'max_bill', 'driver_commission_type', 'driver_flat_type',
        'driver_commission_value', 'driver_commission_value_per', 'taxi_c_commission_type', 'taxi_c_commission_flat_type', 'taxi_c_commission_value', 'taxi_c_commission_value_per', 'hotel_commission_type',
        'hotel_flat_type', 'hotel_commission_value', 'hotel_commission_value_per', 'is_surge_charge', 'surge_charge_type', 'surge_charge_value', 'payment_method',
        'is_insurance', 'insurance_type', 'insurance_value', 'is_active', 'created_by', 'updated_by'
    ];

    public function getPricingCards($request){

        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function scopeOfSearch($query, $request){

        $name = $request->query('name');

        if (!empty($name)) {
            $query->where('name', 'LIKE', '%' . $name . '%');
        }
        return $query;
    }

    public function serviceArea(){
        return $this->belongsTo(ServiceArea::class);
    }

    public function serviceType(){
        return $this->belongsTo(ServiceType::class);
    }

    public function vehicleType(){
        return $this->belongsTo(VehicleType::class);
    }

    public function fareType(){
        return $this->belongsTo(FareType::class);
    }

    public function pricingCardSlots(){
       return $this->hasMany(PricingCardSlot::class);
    }

}

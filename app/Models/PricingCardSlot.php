<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class PricingCardSlot extends Model
{
    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'pricing_card_id', 'parameter_name', 'week_days', 'start_time', 'end_time', 'time_type', 'charge',
        'charge_type', 'is_active', 'created_by', 'updated_by'
    ];

    public function getPricingCardSlots($request){

        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function scopeOfSearch($query, $request){

        $name = $request->query('name');
        if (!empty($name)) {
            $query->where('parameter_name', 'LIKE', '%' . $name . '%');
        }
        return $query;
    }

    public function pricingCard(){
        return $this->belongsTo(PricingCard::class);
    }

}

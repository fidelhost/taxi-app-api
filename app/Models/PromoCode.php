<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromoCode extends Model
{
    use UsesUuid, SoftDeletes, HasFactory;

    protected $fillable = [
        'service_area_id', 'promo_codes_setup', 'promo_type', 'discount', 'description', 'valid_for',
        'applicable_for', 'promo_code_limit', 'promo_code_limit_per_rider', 'promo_Percentage_max_discount', 'pricing_parameter_name',
        'start_date', 'end_date', 'corporate_id', 'is_active', 'created_by', 'updated_by'
    ];

    public function getPromoCodes($request){

        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function scopeOfSearch($query, $request){

        $promo_code = $request->query('promo_code');

        if (!empty($promo_code)) {
            $query->where('promo_codes_setup', '=', $promo_code);
        }
        return $query;
    }

    public function serviceArea(){
        return $this->belongsTo(ServiceArea::class);
    }

    public function pricingCard(){
        return $this->belongsToMany(PricingCard::class)->withPivot('id');
    }
}

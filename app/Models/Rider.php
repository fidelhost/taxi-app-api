<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Rider extends Model
{
    protected $table = 'riders';
}

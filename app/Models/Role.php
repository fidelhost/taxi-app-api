<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;
use App\Models\User;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name','slug'];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission','role_id','permission_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user','role_id','user_id');
    }
}

<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceArea extends Model
{

    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'country_id', 'name', 'timezone', 'polygon_area_of_map', 'pool_service',
        'position_of_pool_on_home_screen',  'driver_min_wallet_balance', 'driver_bill_period', 'driver_bill_start_time',
        'driver_bill_start_day', 'is_airport', 'is_auto_upgrate', 'created_by', 'updated_by'
    ];

    public function getServiceAreas($request){

        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function scopeOfSearch($query, $request){

        $search = $request->query('search');

        if (!empty($search)) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%');
            });
        }
        return $query;
    }

    public function country()
    {
        return  $this->hasOne(Country::class, 'id','country_id');
    }

    public function vehicleType(){
        return $this->belongsToMany(VehicleType::class)->withPivot('id');
    }

    public function normalServiceVehicleType(){
        return $this->belongsToMany(VehicleType::class , 'normal_services')->withPivot('id');
    }

    public function outstationServiceVehicleType(){
        return $this->belongsToMany(VehicleType::class , 'outstation_services')->withPivot('id');
    }

//    public function rentalServiceVehicleType(){
//        return $this->belongsToMany(Package::class , 'rental_services')->withPivot('id');
//    }

    public function driverDocs(){
        return $this->belongsToMany(Document::class , 'service_area_driver_docs')->withPivot('id');
    }

    public function vehicleDocs(){
        return $this->belongsToMany(Document::class , 'service_area_vehicle_docs')->withPivot('id');
    }
}

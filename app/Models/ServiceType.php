<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceType extends Model
{
    use UsesUuid, SoftDeletes;

    protected $fillable = [
        'type', 'name', 'color', 'icon', 'is_active', 'created_by', 'updated_by'
    ];

    public function getServiceTypes($request){

        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function scopeOfSearch($query, $request){

        $type = $request->query('type');
        $name = $request->query('name');

        if (!empty($name)) {
            $query->where('name', 'LIKE', '%' . $name . '%');
        }
        if (!empty($type)) {
            $query->where('type', 'LIKE', $name);
        }
        return $query;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SiteMenu extends Model
{
    protected $table = 'site_menus';

    public function child()
    {
        return $this->hasMany(SiteMenu::class, 'parent_id', 'id')->with('child');
    }

    public function parent()
    {
        return $this->hasOne(SiteMenu::class, 'id', 'parent_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trans extends Model
{
    protected $table = 'translations';
    protected $fillable = ['key','text','lang_code','group','sub_group','device'];

}

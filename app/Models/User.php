<?php

namespace App\Models;

use App\Models\Concerns\UsesUuid;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;



class User extends Authenticatable
{

    use HasApiTokens, Notifiable, UsesUuid, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email','phone','gender','image', 'nid_number', 'referral_code', 'is_accepted_tc', 'password', 'ip_address', 'last_login_at',
        'last_login_ip_address', 'type', 'is_active', 'is_ban', 'is_approve', 'is_reject'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getUsers($request)
    {
        return $this->activeUsers()->ofSearch($request);
    }

    public function getUserList($request){
        return $this->ofSearch($request)
            ->orderBy('created_at', config('settings.pagination.order_by'))
            ->paginate(config('settings.pagination.per_page'));
    }

    public function activeUsers()
    {
        return $this->where('is_active', 1);
    }

    public function scopeOfSearch($query, $request){

        $search = $request->query('search');
        $active = $request->query('is_active');
        $type = $request->query('type');
        $email = $request->query('email');

        if ($request->has('is_active')) {
            $query->where('is_active', $active);
        }
        if(!empty($email)){
            $query->where('email', $email);
        }
        if(!empty($type)){
            $query->where('type', $type);
        }
        if (!empty($search)) {
            $query->where(function ($q) use ($search) {
                $q->where('name', 'LIKE', '%' . $search . '%');
            });
        }
        return $query;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Carbon\Carbon;
class WalletTransaction extends Model
{
    protected $table = 'wallet_transactions';
    protected $appends = [
        'create_time',
    ];

    public function senderInfo()
    {
        return $this->hasOne(User::class, 'id', 'sender');
    }

    public function receiverInfo()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getCreateTimeAttribute(){
        $carbondate = Carbon::parse($this->created_at);
        $past = $carbondate->diffForHumans();
        return $carbondate->isoFormat('MMMM Do YYYY');
    }

}

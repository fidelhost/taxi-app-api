<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Symfony\Component\Finder\Finder;
use Composer\Autoload\ClassLoader;
use Illuminate\Support\Str;

use Config;

class ProjectServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router, \App\Http\Code4mkKernel $Code4mkKernel)
    {

        foreach ($Code4mkKernel->routeMiddleware() as $key => $value) {
          $router->aliasMiddleware($key,$value);
        }

        foreach ($Code4mkKernel->middlewareGroup() as $key => $value) {
          $router->middlewareGroup($key,$value);
        }

        foreach ($Code4mkKernel->prependMiddlewareGroup() as $key => $value) {
          $router->prependMiddlewareToGroup($key,$value);
        }

        foreach ($Code4mkKernel->middleware() as $key => $value) {
          $this->app->make('Illuminate\Contracts\Http\Kernel')->prependMiddleware($value);
        }

        // config register
        // https://medium.com/@koenhoeijmakers/properly-merging-configs-in-laravel-packages-a4209701746d
        $this->mergeConfigFrom(
          __DIR__.'/../../config/permission.php','project-permissions'
        );
        $this->mergeConfigFrom(
          __DIR__.'/../../config/translate.php','project-translate'
        );


        // Manually load helpers
        include __DIR__.'/../Helpers/Taxi.php';


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {



    }
}

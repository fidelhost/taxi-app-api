<?php

return [
    'show-user' => 'Show User',
    'edit-user' => 'Edit User',
    'delete-user' => 'Delete User'
];

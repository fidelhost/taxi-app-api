<?php

return [

    'pagination' => [
        'per_page' => 10,
        'order_by' => 'desc',
    ],

    'message' => [
        'saved' => 'Successfully Saved',
        'updated' => 'Successfully Updated',
        'published' => 'Successfully Published',
        'deleted' => 'Successfully Deleted',
        'status_changed' => 'Status Changed Successfully',
        'not_found' => 'Record Not Found',
        'url_not_found' => 'URL Not Found',
        'bad_request' => 'Bad Request',
        'validation_fail' => 'The given data was invalid.',
    ],
];



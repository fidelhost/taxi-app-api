<?php

return [
    'auth.login' => [
        'text' => 'login',
        'group' => 'auth',
        'sub_group' => '',
        'device' => 'web'
    ],
    'auth.register' => [
        'text' => 'register',
        'group' => 'auth',
        'sub_group' => '',
        'device' => 'web'
    ],
    'taxi.app' => [
        'text' => 'welcome to taxy app',
        'group' => 'home',
        'sub_group' => '',
        'device' => 'web'
    ],
    'taxi.about' => [
        'text' => 'taxi app about',
        'group' => 'home',
        'sub_group' => '',
        'device' => 'web'
    ],
    'taxi.author' => [
        'text' => 'taxi app author',
        'group' => 'home',
        'sub_group' => '',
        'device' => 'web'
    ]
];

<?php

namespace Database\Factories;

use App\Models\FareType;
use Illuminate\Database\Eloquent\Factories\Factory;

class FareTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FareType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

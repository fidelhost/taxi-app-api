<?php

namespace Database\Factories;

use App\Models\VehicleName;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleNameFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VehicleName::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

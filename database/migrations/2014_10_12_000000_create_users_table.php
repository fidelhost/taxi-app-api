<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email',100)->unique();
            $table->string('phone',20)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('type', [
                'SUPER_ADMIN', 'ADMIN','DRIVER','MERCHANT','PASSENGER','GUEST'
            ])->default('GUEST');
            $table->string('image',255)->nullable();
            $table->string('nid_number',25)->unique();
            $table->enum('gender',['MALE','FEMALE','OTHER'])->default('MALE');
            $table->string('referral_code',100)->nullable();
            $table->boolean('is_accepted_tc')->default(false);
            $table->boolean('is_active')->default(false);
            $table->boolean('is_ban')->default(false);
            $table->boolean('is_approve')->default(false);
            $table->boolean('is_reject')->default(false);
            $table->rememberToken();
            $table->string('ip_address')->nullable();
            $table->string('last_login_ip_address')->nullable();
            $table->dateTime('last_login_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->index('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

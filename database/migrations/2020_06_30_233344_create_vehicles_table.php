<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('driver_id');
            $table->string('service_type');
            $table->string('v_type');
            $table->string('v_no');
            $table->string('v_id');
            $table->string('v_color');
            $table->string('v_image');
            $table->string('v_image_numberplate');
            $table->boolean('is_approved')->default(false);
            $table->boolean('is_verified')->default(false);
            $table->boolean('is_rejected')->default(false);
            $table->boolean('is_ban')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}

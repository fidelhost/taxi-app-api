<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->text('code');
            $table->text('type'); // per,fixed
            $table->float('amount', 8, 2);
            $table->float('max_amount', 8, 2)->nullable();
            $table->text('quantity')->nullable();
            $table->text('per_user_quantity')->nullable();
            $table->date('expired')->nullable();
            $table->date('active_on')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_site_settings', function (Blueprint $table) {
            $table->id();
            $table->text('logo')->nullable();
            $table->text('site_name')->nullable();
            $table->text('copyright')->nullable();
            $table->text('email')->nullable();
            $table->text('contact')->nullable();
            $table->text('addrress')->nullable();
            $table->text('social')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_site_settings');
    }
}

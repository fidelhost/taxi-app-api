<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_areas', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('country_id');
            $table->string('name')->unique();
            $table->string('timezone');
            $table->string('polygon_area_of_map')->nullable();
            $table->boolean('pool_service')->nullable();
            $table->string('position_of_pool_on_home_screen');
            $table->double('driver_min_wallet_balance',15,2)->nullable();
            $table->string('driver_bill_period')->nullable();
            $table->string('driver_bill_start_time')->nullable();
            $table->string('driver_bill_start_day')->nullable();
            $table->dateTime('driver_bill_start_date')->nullable();
            $table->boolean('is_airport')->default(false);
            $table->boolean('is_auto_upgrate')->default(false);
            $table->boolean('is_active')->default(true);
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_areas');
    }
}

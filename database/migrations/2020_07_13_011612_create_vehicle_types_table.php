<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_types', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('vehicle_name_id');
            $table->uuid('type_category_id');
            $table->string('name')->unique();
            $table->string('rank')->nullable();
            $table->string('sequence')->nullable();
            $table->string('description')->nullable();
            $table->text('image')->nullable();
            $table->text('selected_image')->nullable();
            $table->text('map_image')->nullable();
            $table->boolean('available_ride')->default(false);
            $table->boolean('available_later')->default(true);
            $table->boolean('available_pool')->default(false);
            $table->boolean('is_active')->default(false);
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_types');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_systems', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('country_id')->nullable();
            $table->string('app')->nullable();
            $table->string('limit_type')->nullable();
            $table->string('number_limit')->nullable();
            $table->string('num_days')->nullable();
            $table->string('day_count')->nullable();
            $table->string('applicible')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->date('offer_type')->nullable();
            $table->string('offer_value_type')->nullable();
            $table->string('offer_value')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_systems');
    }
}

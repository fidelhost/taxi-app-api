<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('driver_id')->nullable();
            $table->bigInteger('rider_id')->nullable();
            $table->bigInteger('ride_type_id')->nullable();
            $table->bigInteger('area_id')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->float('rent_amount', 8, 2)->nullable();
            $table->boolean('ride_complete')->default(false);
            $table->string('status')->nullable();
            $table->date('ride_complete_time')->nullable();
            $table->string('payment_method')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rides');
    }
}

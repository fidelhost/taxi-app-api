<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->enum('mandatory', ['Yes', 'No'])->default('Yes');
            $table->enum('expired', ['Yes', 'No'])->default('Yes');
            $table->enum('required', ['Enable', 'Disable'])->default('Enable');
            $table->enum('type',['Driver', 'Vehicle'])->nullable();
            $table->boolean('is_active')->default(false);
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}

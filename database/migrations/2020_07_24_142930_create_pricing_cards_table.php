<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_cards', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->uuid('service_area_id');
            $table->uuid('service_type_id');
            $table->uuid('fare_type_id')->nullable();
            $table->uuid('vehicle_type_id')->nullable();
            $table->uuid('package_id')->nullable();
            $table->integer('is_applicable_all_type_vehicles')->nullable();
            $table->string('bill_type')->nullable();
            $table->float('time_charge', 8, 2)->nullable();
            $table->float('bare_fare', 8, 2)->nullable();
            $table->float('waiting_time', 8, 2)->nullable();
            $table->float('distance_charge', 8, 2)->nullable();
            $table->float('distance_charges', 8, 2)->nullable();
            $table->string('distance_bare_fare')->nullable();
            $table->string('time_bare_fare')->nullable();
            $table->string('fare_time_waiting_charge')->nullable();
            $table->float('u_min_wallet_balance', 8, 2)->nullable();
            $table->float('extra_seat_charge', 8, 2)->nullable();
            $table->float('max_distance', 8, 2)->nullable();
            $table->float('max_bill', 8, 2)->nullable();
            $table->string('driver_commission_type')->nullable();
            $table->string('driver_flat_type')->nullable();
            $table->string('driver_commission_value')->nullable();
            $table->string('driver_commission_value_per')->nullable();
            $table->string('taxi_c_commission_type')->nullable();
            $table->string('taxi_c_commission_flat_type')->nullable();
            $table->string('taxi_c_commission_value')->nullable();
            $table->string('taxi_c_commission_value_per')->nullable();
            $table->string('hotel_commission_type')->nullable();
            $table->string('hotel_flat_type')->nullable();
            $table->string('hotel_commission_value')->nullable();
            $table->string('hotel_commission_value_per')->nullable();
            $table->boolean('is_surge_charge')->default(false);
            $table->string('surge_charge_type')->nullable();
            $table->string('surge_charge_value')->nullable();
            $table->string('payment_method')->nullable();
            $table->boolean('is_insurance')->nullable();
            $table->string('insurance_type')->nullable();
            $table->float('insurance_value', 8, 2)->nullable();
            $table->boolean('is_active')->default(false);
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_cards');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingCardSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_card_slots', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('pricing_card_id');
            $table->string('parameter_name')->nullable();
            $table->text('week_days')->nullable();
            $table->string('start_time')->nullable();
            $table->string('end_time')->nullable();
            $table->string('time_type')->nullable();
            $table->float('charge', 8, 2);
            $table->string('charge_type')->nullable();
            $table->boolean('is_active')->default(false);
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_card_slots');
    }
}

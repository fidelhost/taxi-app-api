<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_codes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('service_area_id');
            $table->string('promo_codes_setup');
            $table->enum('promo_type',['Flat Rate', 'Percentage'])->default('Percentage');
            $table->float('discount', 8, 2)->nullable();
            $table->text('description')->nullable();
            $table->enum('valid_for',['Always', 'Custom Date'])->default('Always');
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->uuid('applicable_for')->nullable();
            $table->string('promo_code_limit')->nullable();
            $table->string('promo_code_limit_per_rider')->nullable();
            $table->string('promo_Percentage_max_discount')->nullable();
            $table->string('pricing_parameter_name')->nullable();
            $table->uuid('corporate_id')->nullable();
            $table->boolean('is_active')->default(false);
            $table->uuid('created_by');
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_codes');
    }
}

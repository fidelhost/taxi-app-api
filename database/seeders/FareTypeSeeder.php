<?php

namespace Database\Seeders;

use App\Models\FareType;
use Illuminate\Database\Seeder;

class FareTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FareType::create(['name' => 'Other City (Round Trip Only)', 'is_active' => true]);
        FareType::create(['name' => 'Special City (One Way)', 'is_active' => true]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::create(['type' => 'Package', 'name' => 'Package', 'description' => '', 'term_and_condition' => '', 'is_active' => true]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\ParameterType;
use Illuminate\Database\Seeder;

class ParameterTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ParameterType::create(['name' => 'Base Fare Type', 'is_active' => true]);
        ParameterType::create(['name' => 'Per Mile/Km', 'is_active' => true]);
        ParameterType::create(['name' => 'Per Min', 'is_active' => true]);
        ParameterType::create(['name' => 'Wait Type(Per Min)', 'is_active' => true]);
        ParameterType::create(['name' => 'Standard', 'is_active' => true]);
    }
}

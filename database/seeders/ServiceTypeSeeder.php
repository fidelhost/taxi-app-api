<?php

namespace Database\Seeders;

use App\Models\ServiceType;
use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceType::create(['type' => 'Normal', 'name' => 'Daily Rides', 'color' => '#ffff00', 'icon' => '', 'is_active' => true]);
        ServiceType::create(['type' => 'Rental', 'name' => 'Intercity', 'color' => '#21a1de', 'icon' => '', 'is_active' => true]);
        ServiceType::create(['type' => 'Outstation', 'name' => 'Long dist', 'color' => '#000000', 'icon' => '', 'is_active' => true]);
        ServiceType::create(['type' => 'Pool', 'name' => 'Pool', 'color' => '#ffffff', 'icon' => '', 'is_active' => true]);
    }
}

<?php

use App\Http\Controllers\CountryController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\PricingCardController;
use App\Http\Controllers\PricingParameterController;
use App\Http\Controllers\PromoCodeController;
use App\Http\Controllers\ServiceAreaController;
use App\Http\Controllers\ServiceTypeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleMakeController;
use App\Http\Controllers\VehicleModelController;
use App\Http\Controllers\VehicleNameController;
use App\Http\Controllers\VehicleTypeCategoryController;
use App\Http\Controllers\VehicleTypeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Web\V1\AreaController;
use App\Http\Controllers\Api\Web\V1\I18nController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::post('login', [UserController::class, 'login']);
    Route::post('register', [UserController::class, 'store']);
    Route::get('vehicle-information/{id}', function (){
        return '{
                    "items": [
                        {
                            "id": "1",
                            "name": "Economy",
                            "vehicle_name": [
                                {
                                    "id": "1",
                                    "name": "JustGrab",
                                    "image": "12",
                                    "seat": "1-4 seat"
                                }
                            ]
                        },
                        {
                            "id": "2",
                            "name": "Premium",
                            "vehicle_name": [
                                {
                                    "id": "2",
                                    "name": "JustGrab",
                                    "image": "12",
                                    "seat": "1-4 seat"
                                }
                            ]
                        }
                    ]
                }';
    });

    Route::group(['middleware' => 'auth:api'], function () {


        // ...............User.............
        Route::prefix('users')->group(function () {
            Route::post('/', [UserController::class, 'store'])->name('store-user');
            Route::put('/{uuid}', [UserController::class, 'update'])->name('update-user');
            Route::get('/list', [UserController::class, 'userList'])->name('users');
            Route::get('/{uuid}', [UserController::class, 'show'])->name('user');
            Route::get('/', [UserController::class, 'index'])->name('users');
            Route::delete('/{uuid}', [UserController::class, 'destroy'])->name('delete-user');
        });

        Route::prefix('countries')->group(function () {
            Route::get('/', [CountryController::class, 'index'])->name('countries');
            Route::get('/{uuid}', [CountryController::class, 'show'])->name('country');
            Route::post('/', [CountryController::class, 'store'])->name('store-country');
            Route::put('/{uuid}', [CountryController::class, 'update'])->name('update-country');
            Route::delete('/{uuid}', [CountryController::class, 'destroy'])->name('delete-country');
//            Route::patch('/change-status/{uuid}', [CountryController::class, 'changeStatus'])->name('change-status-country');
        });

        Route::prefix('service-areas')->group(function () {
            Route::get('/', [ServiceAreaController::class, 'index'])->name('service-areas');
            Route::get('/{uuid}', [ServiceAreaController::class, 'show'])->name('service-area');
            Route::post('/', [ServiceAreaController::class, 'store'])->name('store-service-area');
            Route::put('/{uuid}', [ServiceAreaController::class, 'update'])->name('update-service-area');
            Route::delete('/{uuid}', [ServiceAreaController::class, 'destroy'])->name('delete-service-area');
        });

        Route::prefix('service-types')->group(function () {
            Route::get('/', [ServiceTypeController::class, 'index'])->name('service-types');
            Route::get('/{uuid}', [ServiceTypeController::class, 'show'])->name('service-type');
            Route::post('/', [ServiceTypeController::class, 'store'])->name('store-service-type');
            Route::post('/{uuid}', [ServiceTypeController::class, 'update'])->name('update-service-type');
            Route::delete('/{uuid}', [ServiceTypeController::class, 'destroy'])->name('delete-service-type');
        });

        Route::prefix('vehicle-names')->group(function () {
            Route::get('/', [VehicleNameController::class, 'index'])->name('vehicle-names');
            Route::get('/{uuid}', [VehicleNameController::class, 'show'])->name('vehicle-name');
            Route::post('/', [VehicleNameController::class, 'store'])->name('store-vehicle-name');
            Route::put('/{uuid}', [VehicleNameController::class, 'update'])->name('update-vehicle-name');
            Route::delete('/{uuid}', [VehicleNameController::class, 'destroy'])->name('delete-vehicle-name');
        });

        Route::prefix('vehicle-type-categories')->group(function () {
            Route::get('/', [VehicleTypeCategoryController::class, 'index'])->name('vehicle-type-categories');
            Route::get('/{uuid}', [VehicleTypeCategoryController::class, 'show'])->name('vehicle-type-category');
            Route::post('/', [VehicleTypeCategoryController::class, 'store'])->name('store-vehicle-type-category');
            Route::put('/{uuid}', [VehicleTypeCategoryController::class, 'update'])->name('update-vehicle-type-category');
            Route::delete('/{uuid}', [VehicleTypeCategoryController::class, 'destroy'])->name('delete-vehicle-type-category');
        });

        Route::prefix('vehicle-types')->group(function () {
            Route::get('/', [VehicleTypeController::class, 'index'])->name('vehicle-types');
            Route::get('/{uuid}', [VehicleTypeController::class, 'show'])->name('vehicle-type');
            Route::post('/', [VehicleTypeController::class, 'store'])->name('store-vehicle-type');
            Route::put('/{uuid}', [VehicleTypeController::class, 'update'])->name('update-vehicle-type');
            Route::delete('/{uuid}', [VehicleTypeController::class, 'destroy'])->name('delete-vehicle-type');
        });

        Route::prefix('vehicle-models')->group(function () {
            Route::get('/', [VehicleModelController::class, 'index'])->name('vehicle-models');
            Route::get('/{uuid}', [VehicleModelController::class, 'show'])->name('vehicle-model');
            Route::post('/', [VehicleModelController::class, 'store'])->name('store-vehicle-model');
            Route::put('/{uuid}', [VehicleModelController::class, 'update'])->name('update-vehicle-model');
            Route::delete('/{uuid}', [VehicleModelController::class, 'destroy'])->name('delete-vehicle-model');
        });

        Route::prefix('vehicle-makes')->group(function () {
            Route::get('/', [VehicleMakeController::class, 'index'])->name('vehicle-makes');
            Route::get('/{uuid}', [VehicleMakeController::class, 'show'])->name('vehicle-make');
            Route::post('/', [VehicleMakeController::class, 'store'])->name('store-vehicle-make');
            Route::put('/{uuid}', [VehicleMakeController::class, 'update'])->name('update-vehicle-make');
            Route::delete('/{uuid}', [VehicleMakeController::class, 'destroy'])->name('delete-vehicle-make');
        });

        Route::prefix('documents')->group(function () {
            Route::get('/', [DocumentController::class, 'index'])->name('documents');
            Route::get('/{uuid}', [DocumentController::class, 'show'])->name('document');
            Route::post('/', [DocumentController::class, 'store'])->name('store-document');
            Route::put('/{uuid}', [DocumentController::class, 'update'])->name('update-document');
            Route::delete('/{uuid}', [DocumentController::class, 'destroy'])->name('delete-document');
        });

        Route::prefix('pricing-parameters')->group(function () {
            Route::get('/', [PricingParameterController::class, 'index'])->name('pricing-parameters');
            Route::get('/{uuid}', [PricingParameterController::class, 'show'])->name('pricing-parameter');
            Route::post('/', [PricingParameterController::class, 'store'])->name('store-pricing-parameter');
            Route::put('/{uuid}', [PricingParameterController::class, 'update'])->name('update-pricing-parameter');
            Route::delete('/{uuid}', [PricingParameterController::class, 'destroy'])->name('delete-pricing-parameter');
        });

        Route::prefix('pricing-cards')->group(function () {
            Route::get('/', [PricingCardController::class, 'index'])->name('service-areas');
            Route::get('/{uuid}', [PricingCardController::class, 'show'])->name('service-area');
            Route::post('/', [PricingCardController::class, 'store'])->name('store-service-area');
            Route::put('/{uuid}', [PricingCardController::class, 'update'])->name('update-service-area');
            Route::delete('/{uuid}', [PricingCardController::class, 'destroy'])->name('delete-service-area');
        });

        Route::prefix('promo-codes')->group(function () {
            Route::get('/', [PromoCodeController::class, 'index'])->name('promo-codes');
            Route::get('/{uuid}', [PromoCodeController::class, 'show'])->name('promo-code');
            Route::post('/', [PromoCodeController::class, 'store'])->name('store-promo-code');
            Route::put('/{uuid}', [PromoCodeController::class, 'update'])->name('update-promo-code');
            Route::delete('/{uuid}', [PromoCodeController::class, 'destroy'])->name('delete-promo-code');
        });

    });


    Route::get('i18n/{param}', function($param){
        $path = resource_path('lang/'.$param);
        $file = File::get($path);
        $response = Response::make($file, 200);
        return $response;
    })->where('param','.*');

    Route::prefix('lang')->group(function () {
        Route::get('create',[I18nController::class, 'createLang']);
        Route::get('get', [I18nController::class, 'get']);
        Route::get('activity', [I18nController::class, 'activity']);
    });



    Route::prefix('area')->group(function () {
        Route::post('create',[AreaController::class, 'createArea']);
        Route::post('update',[AreaController::class, 'updateArea']);
        Route::get('get/{uuid}', [AreaController::class, 'show']);
        Route::get('get',[AreaController::class, 'get']);
        Route::post('action',[AreaController::class, 'action']);
    });

    Route::prefix('package')->group(function () {
        Route::post('create','Api\Web\V1\PackageController@createPackage');
        Route::post('update','Api\Web\V1\PackageController@updatePackage');
        Route::get('get','Api\Web\V1\PackageController@get');
        Route::post('action','Api\Web\V1\PackageController@action');
    });

    Route::prefix('coupon')->group(function () {
        Route::post('create','Api\Web\V1\CouponController@createCoupon');
        Route::get('get','Api\Web\V1\CouponController@get');
    });

    Route::prefix('translate')->group(function () {
        Route::get('get','Api\Web\V1\I18nController@nowTranslate');
        Route::get('get-the-translations','Api\Web\V1\I18nController@getTheTranslations');
        Route::get('update-the-translations','Api\Web\V1\I18nController@updateTheTranslations');
        Route::get('generate','Api\Web\V1\I18nController@generateJson');

    });

    Route::prefix('wallet')->group(function () {
        Route::get('transactions','Api\Web\V1\WalletController@get');
        Route::post('add-transaction','Api\Web\V1\WalletController@addTransaction');
    });

    Route::prefix('points')->group(function () {
        Route::get('get-condition','Api\Web\V1\PointController@getCondition');
        Route::post('condition','Api\Web\V1\PointController@condition');
    });

    Route::prefix('setting')->group(function () {
        Route::prefix('website')->group(function(){
            Route::post('create','Api\Web\V1\WebsiteSettingController@create');
            Route::get('get','Api\Web\V1\WebsiteSettingController@get');
        });

        Route::post('create-meta','Api\Web\V1\MetaController@create');
        Route::get('get-meta','Api\Web\V1\MetaController@get');
    });


    Route::prefix('page')->group(function () {
        Route::get('get','Api\Web\V1\PageController@get');
        Route::post('create','Api\Web\V1\PageController@createPage');
        Route::post('update','Api\Web\V1\PageController@updatePage');
    });

    Route::prefix('navigation')->group(function () {
        Route::get('get','Api\Web\V1\NavigationController@get');
        Route::post('create','Api\Web\V1\NavigationController@create');
        Route::post('update','Api\Web\V1\NavigationController@update');
    });

    Route::prefix('menu')->group(function () {
        Route::get('get','Api\Web\V1\MenuController@get');
        Route::post('create','Api\Web\V1\MenuController@createMenu');
        Route::post('update','Api\Web\V1\MenuController@updateMenu');
    });


    Route::get('register','Api\LoginController@register');
    Route::get('login','Api\LoginController@login');
    Route::get('authid','Api\LoginController@authID');
    Route::get('auth-user','Api\LoginController@authUser');

    Route::prefix('user')->group(function () {
        Route::get('get','Api\Web\V1\UserController@get');
        Route::post('create','Api\Web\V1\UserController@createUser');
        Route::post('update','Api\Web\V1\UserController@updateUser');
        Route::get('all-user-id','Api\Web\V1\UserController@getAllId');
        Route::get('permissions','Api\Web\V1\UserController@getPermissions');
    });

    Route::get('about',function(){
        $msg = ['status' => 'success', 'message' => 'retrive successfully', 'success' => true];
        $data = ['name'=>'kamal'];
        return response()->json(Helper::api_output($msg,$data),200);
    });

    Route::middleware(['web'])->prefix('role')->group(function () {
        Route::get('create','Api\Web\V1\PermissionController@createRole');
        Route::get('get','Api\Web\V1\PermissionController@getRoles');
        Route::post('assign','Api\Web\V1\PermissionController@assignRole');
    });

    Route::get('permissions','Api\Web\V1\PermissionController@get');
    Route::get('permissions/assign','Api\Web\V1\PermissionController@assignPermission');


    Route::prefix('driver')->group(function () {
        Route::get('get','Api\Web\V1\DriverController@getDrivers');
        Route::get('action','Api\Web\V1\DriverController@action');
    });

    Route::prefix('setup')->group(function () {

        Route::get('get-vehicle', [SetupController::class, 'getVehicle']);
        Route::post('create-vehicle', [SetupController::class, 'createVehicle']);
        Route::post('update-vehicle', [SetupController::class, 'updateVehicle']);

        Route::get('get-pricing-parameter',[SetupController::class, 'getPricingParameter']);
        Route::post('create-pricing-parameter',[SetupController::class, 'createPricingParameter']);

        Route::get('get-pricing-parameter-type',[SetupController::class, 'getPricingParameterType']);
        Route::post('create-pricing-parameter-type',[SetupController::class, 'createPricingParameterType']);

        Route::get('get-vehicle-type',[SetupController::class, 'getVehicleType']);
        Route::post('create-vehicle-type',[SetupController::class, 'createVehicleType']);
        Route::post('update-vehicle-type',[SetupController::class, 'updateVehicleType']);

        Route::get('get-vehicle-type-category','Api\Web\V1\SetupController@getVehicleTypeCategory');
        Route::get('get-vehicle-type-by-category','Api\Web\V1\SetupController@getVehicleTypeByCategory');
        Route::post('create-vehicle-type-category','Api\Web\V1\SetupController@createVehicleTypeCategory');
        Route::post('update-vehicle-type-category','Api\Web\V1\SetupController@updateVehicleTypeCategory');

        Route::get('get-vehicle-name','Api\Web\V1\SetupController@getVehicleName');
        Route::get('get-vehicle-by-name','Api\Web\V1\SetupController@getVehicleTypeByCategory');
        Route::post('create-vehicle-name','Api\Web\V1\SetupController@createVehicleName');
        Route::post('update-vehicle-name','Api\Web\V1\SetupController@updateVehicleName');
        Route::post('delete-vehicle-name','Api\Web\V1\SetupController@deleteVehicleName');

        Route::get('get-vehicle-model',[SetupController::class, 'getVehicleModel']);
        Route::post('create-vehicle-model',[SetupController::class, 'createVehicleModel']);
        Route::post('update-vehicle-model',[SetupController::class, 'updateVehicleModel']);

        Route::get('get-document',[SetupController::class, 'getDocument']);
        Route::post('create-document',[SetupController::class, 'createDocument']);
        Route::post('update-document',[SetupController::class, 'updateDocument']);

        Route::get('get-pricing-card','Api\Web\V1\SetupController@getPriceCard');
        Route::post('create-price-card','Api\Web\V1\SetupController@createPricingCard');
        Route::post('update-price-card','Api\Web\V1\SetupController@updatePricingCard');
        Route::get('get-pricing-card-slot','Api\Web\V1\SetupController@getPriceCardSlot');


        Route::get('get-bill-type-fields','Api\Web\V1\SetupController@getBillFields');

        Route::get('get-service-type','Api\Web\V1\SetupController@getServiceType');
        Route::post('create-service-type','Api\Web\V1\SetupController@createServiceType');
        Route::post('update-service-type','Api\Web\V1\SetupController@updateServiceType');


    });

    Route::prefix('referral')->group(function () {
        Route::get('get-referral-system','Api\Web\V1\ReferralController@get');
        Route::post('create-referral-system','Api\Web\V1\ReferralController@createRferral');
        Route::post('update-referral-system','Api\Web\V1\ReferralController@updateRferral');

    });

    Route::prefix('ride')->group(function () {
        Route::get('get','Api\Web\V1\RideController@get');
    });

    Route::prefix('rating')->group(function () {
        Route::get('get','Api\Web\V1\RatingController@get');
    });

});

//require(__DIR__.'/api/web/v1/web-api.php');
//require(__DIR__.'/api/mobile/v1/mobile-api.php');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


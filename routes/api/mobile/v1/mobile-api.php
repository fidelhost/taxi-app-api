<?php


Route::prefix('v1/m/driver')->group(function () {
    Route::post('register','Api\Mobile\V1\Driver\DriverController@register');
    Route::get('get','Api\Mobile\V1\Driver\DriverController@getDrivers');
    Route::get('action','Api\Mobile\V1\Driver\DriverController@action');
    Route::get('login','Api\Mobile\V1\Driver\DriverController@login');
    Route::get('auth-user','Api\Mobile\V1\Driver\DriverController@authUser');
});






Route::prefix('v1/m/rider')->group(function () {
    Route::post('register','Api\Mobile\V1\Rider\RiderController@register');
    Route::get('get','Api\Mobile\V1\Rider\RiderController@getDrivers');
    Route::get('action','Api\Mobile\V1\Rider\RiderController@action');
    Route::get('login','Api\Mobile\V1\Rider\RiderController@login');
    Route::get('auth-user','Api\Mobile\V1\Rider\RiderController@authUser');
});

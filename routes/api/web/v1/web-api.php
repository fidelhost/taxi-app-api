<?php

use App\Http\Controllers\Api\Web\V1\AreaController;
use App\Http\Controllers\Api\Web\V1\CountryController;
use App\Http\Controllers\Api\Web\V1\I18nController;
use Illuminate\Support\Facades\Route;

Route::prefix('v1/web')->group(function () {
    Route::get('i18n/{param}', function($param){
     $path = resource_path('lang/'.$param);
     $file = File::get($path);
     $response = Response::make($file, 200);
      return $response;
    })->where('param','.*');

    Route::prefix('lang')->group(function () {
        Route::get('create',[I18nController::class, 'createLang']);
        Route::get('get', [I18nController::class, 'get']);
        Route::get('activity', [I18nController::class, 'activity']);
    });

    Route::prefix('country')->group(function () {
        Route::post('create', [CountryController::class, 'createCountry']);
        Route::post('update', [CountryController::class, 'updateCountry']);
        Route::get('get', [CountryController::class, 'get']);
        Route::post('action', [CountryController::class, 'action']);
        // Route::get('activity','Api\Web\V1\I18nController@activity');
    });

    Route::prefix('area')->group(function () {
        Route::post('create',[AreaController::class, 'createArea']);
        Route::post('update',[AreaController::class, 'updateArea']);
        Route::get('get/{uuid}', [AreaController::class, 'show']);
        Route::get('get',[AreaController::class, 'get']);
        Route::post('action',[AreaController::class, 'action']);
    });

    Route::prefix('package')->group(function () {
        Route::post('create','Api\Web\V1\PackageController@createPackage');
        Route::post('update','Api\Web\V1\PackageController@updatePackage');
        Route::get('get','Api\Web\V1\PackageController@get');
        Route::post('action','Api\Web\V1\PackageController@action');
    });

    Route::prefix('coupon')->group(function () {
        Route::post('create','Api\Web\V1\CouponController@createCoupon');
        Route::get('get','Api\Web\V1\CouponController@get');
    });

    Route::prefix('translate')->group(function () {
        Route::get('get','Api\Web\V1\I18nController@nowTranslate');
        Route::get('get-the-translations','Api\Web\V1\I18nController@getTheTranslations');
        Route::get('update-the-translations','Api\Web\V1\I18nController@updateTheTranslations');
        Route::get('generate','Api\Web\V1\I18nController@generateJson');

    });

    Route::prefix('wallet')->group(function () {
        Route::get('transactions','Api\Web\V1\WalletController@get');
        Route::post('add-transaction','Api\Web\V1\WalletController@addTransaction');
    });

    Route::prefix('points')->group(function () {
        Route::get('get-condition','Api\Web\V1\PointController@getCondition');
        Route::post('condition','Api\Web\V1\PointController@condition');
    });

    Route::prefix('setting')->group(function () {
        Route::prefix('website')->group(function(){
            Route::post('create','Api\Web\V1\WebsiteSettingController@create');
            Route::get('get','Api\Web\V1\WebsiteSettingController@get');
        });

        Route::post('create-meta','Api\Web\V1\MetaController@create');
        Route::get('get-meta','Api\Web\V1\MetaController@get');
    });


    Route::prefix('page')->group(function () {
        Route::get('get','Api\Web\V1\PageController@get');
        Route::post('create','Api\Web\V1\PageController@createPage');
        Route::post('update','Api\Web\V1\PageController@updatePage');
    });

    Route::prefix('navigation')->group(function () {
        Route::get('get','Api\Web\V1\NavigationController@get');
        Route::post('create','Api\Web\V1\NavigationController@create');
        Route::post('update','Api\Web\V1\NavigationController@update');
    });

    Route::prefix('menu')->group(function () {
        Route::get('get','Api\Web\V1\MenuController@get');
        Route::post('create','Api\Web\V1\MenuController@createMenu');
        Route::post('update','Api\Web\V1\MenuController@updateMenu');
    });


    Route::get('register','Api\LoginController@register');
    Route::get('login','Api\LoginController@login');
    Route::get('authid','Api\LoginController@authID');
    Route::get('auth-user','Api\LoginController@authUser');

    Route::prefix('user')->group(function () {
        Route::get('get','Api\Web\V1\UserController@get');
        Route::post('create','Api\Web\V1\UserController@createUser');
        Route::post('update','Api\Web\V1\UserController@updateUser');
        Route::get('all-user-id','Api\Web\V1\UserController@getAllId');
        Route::get('permissions','Api\Web\V1\UserController@getPermissions');
    });

    Route::get('about',function(){
        $msg = ['status' => 'success', 'message' => 'retrive successfully', 'success' => true];
        $data = ['name'=>'kamal'];
        return response()->json(Helper::api_output($msg,$data),200);
    });

      Route::middleware(['web'])->prefix('role')->group(function () {
        Route::get('create','Api\Web\V1\PermissionController@createRole');
        Route::get('get','Api\Web\V1\PermissionController@getRoles');
        Route::post('assign','Api\Web\V1\PermissionController@assignRole');
     });

    Route::get('permissions','Api\Web\V1\PermissionController@get');
    Route::get('permissions/assign','Api\Web\V1\PermissionController@assignPermission');


    Route::prefix('driver')->group(function () {
        Route::get('get','Api\Web\V1\DriverController@getDrivers');
        Route::get('action','Api\Web\V1\DriverController@action');
    });

    Route::prefix('setup')->group(function () {

        Route::get('get-vehicle', [SetupController::class, 'getVehicle']);
        Route::post('create-vehicle', [SetupController::class, 'createVehicle']);
        Route::post('update-vehicle', [SetupController::class, 'updateVehicle']);

        Route::get('get-pricing-parameter',[SetupController::class, 'getPricingParameter']);
        Route::post('create-pricing-parameter',[SetupController::class, 'createPricingParameter']);

        Route::get('get-pricing-parameter-type',[SetupController::class, 'getPricingParameterType']);
        Route::post('create-pricing-parameter-type',[SetupController::class, 'createPricingParameterType']);

        Route::get('get-vehicle-type',[SetupController::class, 'getVehicleType']);
        Route::post('create-vehicle-type',[SetupController::class, 'createVehicleType']);
        Route::post('update-vehicle-type',[SetupController::class, 'updateVehicleType']);

        Route::get('get-vehicle-type-category','Api\Web\V1\SetupController@getVehicleTypeCategory');
        Route::get('get-vehicle-type-by-category','Api\Web\V1\SetupController@getVehicleTypeByCategory');
        Route::post('create-vehicle-type-category','Api\Web\V1\SetupController@createVehicleTypeCategory');
        Route::post('update-vehicle-type-category','Api\Web\V1\SetupController@updateVehicleTypeCategory');

        Route::get('get-vehicle-name','Api\Web\V1\SetupController@getVehicleName');
        Route::get('get-vehicle-by-name','Api\Web\V1\SetupController@getVehicleTypeByCategory');
        Route::post('create-vehicle-name','Api\Web\V1\SetupController@createVehicleName');
        Route::post('update-vehicle-name','Api\Web\V1\SetupController@updateVehicleName');
        Route::post('delete-vehicle-name','Api\Web\V1\SetupController@deleteVehicleName');

        Route::get('get-vehicle-model',[SetupController::class, 'getVehicleModel']);
        Route::post('create-vehicle-model',[SetupController::class, 'createVehicleModel']);
        Route::post('update-vehicle-model',[SetupController::class, 'updateVehicleModel']);

        Route::get('get-document',[SetupController::class, 'getDocument']);
        Route::post('create-document',[SetupController::class, 'createDocument']);
        Route::post('update-document',[SetupController::class, 'updateDocument']);

        Route::get('get-pricing-card','Api\Web\V1\SetupController@getPriceCard');
        Route::post('create-price-card','Api\Web\V1\SetupController@createPricingCard');
        Route::post('update-price-card','Api\Web\V1\SetupController@updatePricingCard');
        Route::get('get-pricing-card-slot','Api\Web\V1\SetupController@getPriceCardSlot');


        Route::get('get-bill-type-fields','Api\Web\V1\SetupController@getBillFields');

        Route::get('get-service-type','Api\Web\V1\SetupController@getServiceType');
        Route::post('create-service-type','Api\Web\V1\SetupController@createServiceType');
        Route::post('update-service-type','Api\Web\V1\SetupController@updateServiceType');


    });

    Route::prefix('referral')->group(function () {
        Route::get('get-referral-system','Api\Web\V1\ReferralController@get');
        Route::post('create-referral-system','Api\Web\V1\ReferralController@createRferral');
        Route::post('update-referral-system','Api\Web\V1\ReferralController@updateRferral');

    });

    Route::prefix('ride')->group(function () {
        Route::get('get','Api\Web\V1\RideController@get');
    });

    Route::prefix('rating')->group(function () {
        Route::get('get','Api\Web\V1\RatingController@get');
    });


});
